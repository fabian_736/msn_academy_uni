@extends('landing.layouts.patient.app')
@section('content')
<link rel="stylesheet" href="{{asset('landing/css/profile/index.css')}}">
    <div class="row-reverse">
        <div class="col">
            <label for="" id="profile_title" class="h2 font-weight-bold mx-3">Perfil</label>
            @if(session('success'))
            <div class="alert alert-success">{{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            @endif
            @if(session('message'))
            <div class="alert alert-info">{{session('message')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <label for="" id="password_title" class="h2 font-weight-bold mx-3" style="display: none">Cambia tu
                contraseña</label>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="card card-body">
                <div class="row" id="profile_update">
                    <div class="col d-flex justify-content-center">
                        <div class="row-reverse">
                            <form action="{{ route('perfil.update_image') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <div class="col">
                                    <div class="circle_profile" style="background-image: url('{{ auth()->user()->getFirstMediaUrl("avatar") }}');">
                                        <div class="row-reverse " style="margin-top: 50%">
                                            <div class="col d-flex justify-content-center">
                                                <label for="getFile" style="cursor: pointer">
                                                    <img src="{{ url('landing/svg/camara.svg') }}" alt=""
                                                        class="w-100 mt-5">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col d-flex justify-content-center">
                                            <label for="getFile" class="text-white font-weight-bold lead mt-3 ml-2"
                                                style="cursor: pointer">Subir
                                                foto</label>
                                        </div>
                                    </div>
                                    <input name="profile_image" type="file" id="getFile"
                                        class="form-control @error('profile_image') is-invalid @enderror" />
                                    <div class="text-danger">
                                        @error('profile_image')
                                            <span>{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div><br>
                                <div class="col d-flex justify-content-center">
                                    <button type="submit" class="btn w-50" style="background: #312783">Guardar
                                        imagen</button>
                                </div>
                            </form>
                            <div class="col my-4">
                                <a href="javascript:;" class="btn w-100" id="bottom_password_update"
                                    style="background: #312783">Cambia tu contraseña</a>
                            </div>
                        </div>
                    </div>
                    <div>
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <form action="{{ route('perfil.update') }}" method="POST">
                                @csrf
                                @method('PATCH')
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Nombres</label>
                                    <input required placeholder="Digite su nombre" value="{{ auth()->user()->name }}" type="text" name="name" id="name" class="form-control">
                                </div>
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Apellidos</label>
                                    <input required placeholder="Digite su apellido" value="{{ auth()->user()->surname }}" type="text" name="surname" id="surname" class="form-control">
                                </div>
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Número de documento</label>
                                    <input required placeholder="Digite su numero de documento" value="{{ auth()->user()->patient->document_number }}" type="text" name="document_number" id="document_number" class="form-control">
                                </div>
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Dirección de residencia</label>
                                    <input required placeholder="Digite su dirección" value="{{ auth()->user()->patient->address }}" type="text" name="address" id="address" class="form-control">
                                </div>
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Número de contacto</label>
                                    <input required placeholder="Digite su numero de contacto" value="{{ auth()->user()->patient->phone }}" type="text" name="phone" id="phone" class="form-control">
                                </div>
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Correo electrónico</label>
                                    <input required placeholder="Digite su Correo" value="{{ auth()->user()->email }}" type="email" name="email" id="email" class="form-control">
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <button type="submit" class="btn btn-danger w-50">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row" id="password_update" style="display: none">
                    <div class="col-8 mx-auto p-4">
                        <form action="{{ route('user-password.update') }}" method="post">
                            @csrf
                            @method('put')
                            <div class="row-reverse">
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Contraseña actual</label>
                                    <input required placeholder="Digita tu contraseña" name="current_password" type="password" class="form-control">
                                </div>
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Nueva contraseña</label>
                                    <input required placeholder="Digita tu nueva contraseña" name="password" type="password" class="form-control">
                                </div>
                                <div class="col">
                                    <label for="" class="text-primary font-weight-bold lead">Confirmación nueva
                                        contraseña</label>
                                    <input required placeholder="Confirma tu contraseña" name="password_confirmation" type="password" class="form-control">
                                </div>
                                <div class="col d-flex justify-content-center">
                                    <button type="submit" class="btn btn-danger">Cambiar mi contraseña</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
