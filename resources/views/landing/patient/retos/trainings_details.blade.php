@extends('landing.layouts.patient.app')
@section('content')
<div class="row">
    <div class="col-9">
        <label for="" class="h2 font-weight-bold mx-3">{{ $patientTraining->training->name }} - Temas </label>
    </div>
    <div class="col float-right">
        <h3><b> <i style="color:#312783" class="fas fa-award mr-2"></i>Puntos: {{$patientTraining->courses_sum_points}}</b></h3>
    </div>
</div>
@if (session('message'))
<canvas class="confetti confetti2" id="canvas"></canvas>
@endif
<div class="row">
    @foreach ($trainingCourses as $course)
    @if ($course->status == 'Publicado')
    <div class="col-6  container-fluid contenedor text-center">
        <div class="container text-center">
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 container_foto ">
                <div class="ver_mas text-center p-3">
                    <div class="row">
                        <div class="col-sm-4 text-center">
                            {{-- <a style="color: #312783" href="{{ $course->getFirstMediaUrl('media') }}" target="_blank">
                            <p class="p-0 m-0 h5 font-weight-bold"><i class="fa fa-file-pdf mr-2"></i> Ver PDF</p>
                            </a> --}}
                            <a style="color: #312783" href="{{ $course->URL }}" target="_blank">
                                <p class="p-0 m-0 h5 font-weight-bold"><i class="fa fa-file-pdf mr-2"></i> Ver PDF</p>
                            </a>
                            <br>
                        </div>
                        <div class="col-sm-8 text-center">
                            @if ($course->questions->count() >= 1)
                            @php
                            $patientCourse=$patientTraining->courses->firstWhere("course_id", $course->id);
                            @endphp
                            @if (empty($patientCourse?->end_date))
                            <a href="{{ route('modules.questionnaire', ['patientTraining' => $patientTraining, 'course' => $course]) }}">
                                <p class="p-0 m-0 h5 font-weight-bold text-primary"><i class="fa fa-arrow-alt-circle-right mr-2"></i> Ir al cuestionario</p>
                            </a>
                            @else
                            <a href="{{ route('modules.questionnaire.retry', ['patientTraining' => $patientTraining, 'patientCourse' => $patientCourse]) }}">
                                <p class="p-0 m-0 h5 font-weight-bold text-primary"><i class="fa fa-check-circle mr-2"></i> Reintentar</p>
                            </a>
                            @endif
                            @else
                            <p class="p-0 m-0 h5 font-weight-bold text-primary"><i class="fa fa-lock"></i> El curso no esta disponible</p>
                            @endif
                        </div>
                    </div>
                </div>
                <article class="text-left p-4 mt-2">
                    <h2>{{ $course->name }}</h2>
                </article>
                <img src="{{ $course->getFirstMediaUrl('banner') }}" alt="img-blur-shadow" class="img-fluid shadow border-radius-lg">
            </div>
        </div>
    </div>
    @endif
    @endforeach
</div>

<div class="row">
    <div class="col d-flex justify-content-center">
        <a href="{{ route('modules.index') }}" class="btn btn-primary text-white w-25">Regresar</a>
    </div>
</div>

<div class="modal fade" id="end_course" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col d-flex justify-content-center">
                        <label for="" class="font-weight-bold lead text-center" style="color: #312783;">Has terminado el
                            Curso <br> {{ auth()->user()->name }}</label>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-3 p-0 d-flex justify-content-end align-items-center">
                        <div class="fileinput fileinput-new text-center m-0" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-circle img-raised d-flex justify-content-center align-items-center" style="background: linear-gradient(90deg, rgba(49,39,131,1) 0%, rgba(227,6,19,1) 100%); width: 200px; height: 100px;">
                                <a href="javascript:;" class="h4 font-weight-bold text-white">
                                    @if (session('puntaje'))
                                    {{ session('puntaje') }} <br> Puntos
                                    @else
                                    0 <br> Puntos
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 p-0 m-0">
                        <div class="row-reverse p-0 m-0">
                            <div class="col m-0 p-0">
                                <label for="" class="h3 text-center ml-2">
                                    @if (session('message'))
                                    Tienes {{ session('example') }}
                                    Respuestas Correctas de {{ session('message') }}
                                    @endif
                                </label>
                            </div>
                            <div class="col m-0 p-0">
                                <hr class="m-0 p-0" style="width: 100%; height: 2px; border-style: solid; border-color: #312783; float: left">
                            </div>
                        </div>


                    </div>
                    <div class="row-reverse ml-auto mt-3 mr-3">
                        <div class="col mx-auto">
                            <a id="btn-close" class="btn text-white " style="background: #312783;" data-toggle="modal" data-target="#exampleModal2" data-dismiss="modal">SIGUIENTE CURSO</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
@if (session('message'))
<script src="{{asset('landing/js/retos/trainings-details.js')}}"></script>
@endif
@endsection