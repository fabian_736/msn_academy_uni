@extends('landing.layouts.patient.app')
@section('content')
<link rel="stylesheet" href="{{asset('landing/css/retos/tranings.css')}}">
<div class="row mt-3 mb-5">
    <div class="col-10 mx-auto">
        <label for="" class="h2 font-weight-bold mx-3">Cursos </label>
    </div>
</div>

<div class="row" id="content_one">
    <div class="col-10 mx-auto">
        @if (count($trainings) == 0)
        <x-modal-alert message='Cursos' />
        @endif
        @foreach ($trainings as $training)
        <div class="row-reverse">
            <div class="col card card-body p-0 m-0 mb-5">
                <div class="row">
                    <div class="col-4 imgcard" style="border-radius: 10px;background-position: center; background-image: url('{{ $training->getFirstMediaUrl('banner') }}');">
                    </div>
                    <div class="col p-3">
                        <div class="row-reverse">
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="col">
                                        <label for="" class="h3 font-weight-bold">Curso: {{ $training->name }}</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <span title="hay {{ count($training->courses) }} cursos disponibles " class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-secondary">
                                        {{ count($training->courses) }}
                                    </span>
                                </div>
                            </div>
                            <div class="col " style="min-height: 13vh; max-height: 13vh; overflow-y: auto">
                                <p class="h4">{{ $training->description }}</p>
                            </div>
                            <div class="col my-3">
                                <div>
                                    <p class="h4 font-weight-bold">Fecha de vigencia: {{ Carbon\Carbon::parse($training->created_at)->format('d-m-Y') }}</p>
                                    <!-- <p class="h4 font-weight-bold">
                                        Cursos: {{ count($training->courses)}}
                                    </p> -->
                                </div>
                                <div class="row">
                                    <div class="col d-flex justify-content-end">
                                        @if (count($training->courses)>=1)
                                        <a href="{{ route('modules.verify',$training ) }}" class="btn btn-primary">Continuar curso</a>
                                        @else
                                        <button disabled class="btn btn-primary">Próximamente</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        {{ $trainings->links() }}
    </div>
</div>


<div class="row " id="content_two" style="display: none">
    <div class="col-10 mx-auto">
        <div class="row-reverse">
            <div class="col card card-body p-0 m-0 mb-5">
                <div class="row">
                    <div class="col p-5">
                        <label for="" class="h4 font-weight-bold">No hay noticias disponibles en la categoria 2</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row " id="content_three" style="display: none">
    <div class="col-10 mx-auto">
        <div class="row-reverse">
            <div class="col card card-body p-0 m-0 mb-5">
                <div class="row">
                    <div class="col p-5">
                        <label for="" class="h4 font-weight-bold">No hay noticias disponibles en la categoria 3</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection