@extends('landing.layouts.patient.app')
@section('content')
<link rel="stylesheet" href="{{asset('landing/css/portal/index.css')}}">
<div class="row-reverse">
    <div class="col">
        <label for="" class="h2 font-weight-bold mx-3">Bienvenido a nuestro programa Pasión por Cuidarte</label>
        <p class="mx-3">Aquí encontrarás:</p>
    </div>
    {{-- <div class="col">
        <div class="col mt-4 mb-5 col-carrousel">
            <div id="carouselExampleIndicators" class="carousel slide " data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active mt-5 li_indicador"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" class="mt-5 li_indicador"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2" class="mt-5 li_indicador"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3" class="mt-5 li_indicador"></li>
                </ol>
                <div class="carousel-inner itemCarrousel">
                    <div class="carousel-item active">
                        <img class="d-block w-100 " src="{{ url('landing/img/banner/1.png') }}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 " src="{{ url('landing/img/banner/2.png') }}" alt="Second slide">
                    </div>

                    <div class="carousel-item">
                        <img class="d-block w-100 " src="{{ url('landing/img/banner/3.png') }}" alt="Third slide">
                    </div>

                    <div class="carousel-item">
                        <img class="d-block w-100 " src="{{ url('landing/img/banner/4.png') }}" alt="Four slide">
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
</div>

<div class="row mx-auto mt-5">
    <div class="col-lg-3 col-md-3 col-sm-10 mx-auto p-0" id="col_cardhome">
        <div id="cardone_home" style="background-image: url('/landing/img/card/1.png');" class="img-fluid">
            <div class="row-reverse w-100 mb-5" style="position: absolute; bottom: 0">
                <div class="col d-flex justify-content-center align-items-center">
                    <h2 class="text-white font-weight-bold">Mis cursos</h2>
                </div>
                <div class="col d-flex justify-content-center align-items-center">
                    <h4 class="text-white text-center">Aprende más sobre tu patología</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-10 mx-auto p-0" id="col_cardhomenew" >
        <div id="cardtwo_home"  style=" background-image:url('/landing/img/card/2.png');" class="img-fluid">
            <div class="row-reverse w-100 mb-5" style="position: absolute; bottom: 0">
                <div class="col d-flex justify-content-center align-items-center">
                    <h2 class="text-white font-weight-bold">Noticias</h2>
                </div>
                <div class="col d-flex justify-content-center align-items-center">
                    <h4 class="text-white text-center ">Tips de salud y comida saludable</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-10 mx-auto p-0" id="col_cardhomeBlog">
        <div id="cardthree_home"  style="background-image: url('landing/img/card/3.png');" class="img-fluid">
            <div class="row-reverse w-100 mb-5" style="position: absolute; bottom: 0">
                <div class="col d-flex justify-content-center align-items-center">
                    <h2 class="text-white font-weight-bold">Blog</h2>
                </div>
                <div class="col d-flex justify-content-center align-items-center">
                    <h4 class="text-white text-center ">Comenta e interactúa con otros usuarios</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('landing/js/portal/index.js')}}"></script>
@endsection
