@extends('landing.layouts.patient.app')
@section('content')
<link rel="stylesheet" href="{{asset('landing/css/noticias/index.css')}}">
<div class="row mt-3 mb-5">
    <div class="col-10 mx-auto">
        <label for="" class="h2 font-weight-bold mx-3">Noticias</label>
    </div>
</div>

<div class="row my-4">
    <div class="col-10 mx-auto">
        <div class="row">
            <div class="col-2 ">
                <a href="javascript:;" class="btn text-white btn-danger h4" id="category_one" style="font-weight: bold">Hogar</a>
            </div>
            <div class="col-2">
                <a href="javascript:;" class="btn text-danger bg-white h4" id="category_two" style="font-weight: bold">Salud</a>
            </div>
            <div class="col-2">
                <a href="javascript:;" class="btn text-danger bg-white h4" id="category_three" style="font-weight: bold">Bienestar</a>
            </div>
        </div>
    </div>
</div>

<div class="row" id="content_one">
    <div class="col-10 mx-auto">
        <div class="row-reverse">
            @foreach ($news as $new)
            <div class="col card card-body p-0 m-0 mb-5">
                <div class="row">
                    <div class="col imgcard" style="border-radius: 10px; background-image: url('{{ $new->getFirstMediaUrl('banner')}}')">
                    </div>

                    <div class="col p-3">
                        <div class="row-reverse">
                            <div class="col">
                                <label for="" class="h3 font-weight-bold">{{ $new->title }}</label>
                            </div>
                            <div class="col">
                                <p class="h4">{{ Str::limit($new->body, 300) }}</p>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col d-flex justify-content-end">
                                        <a href="{{url('new/details/'.$new->id)}}" class="btn btn-primary">Ver Noticia</a>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="col">
                                <p class="h5"><strong>{{ 'Publicado por: '.$new->author->name }}</strong></p>
                            </div>
                            <div class="col">
                                <p class="h5"><strong> Fecha de publicación: {{ Carbon\Carbon::parse($new->created_at)->format('d-m-Y') }}</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        {{ $news->links() }}
    </div>
</div>


<div class="row " id="content_two" style="display: none">
    <div class="col-10 mx-auto">
        <div class="row-reverse">
            <div class="col card card-body p-0 m-0 mb-5">
                <div class="row">
                    <div class="col p-5">
                        <label for="" class="h4 font-weight-bold">No hay noticias disponibles en la categoria 2</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row " id="content_three" style="display: none">
    <div class="col-10 mx-auto">
        <div class="row-reverse">
            <div class="col card card-body p-0 m-0 mb-5">
                <div class="row">
                    <div class="col p-5">
                        <label for="" class="h4 font-weight-bold">No hay noticias disponibles en la categoria 3</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if (count($news) == 0)
<x-modal-alert message='Noticias' />
@endif
<script src="{{asset('landing/js/noticias/index.js')}}"></script>
@endsection