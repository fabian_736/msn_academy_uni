@extends('landing.layouts.patient.app')
@section('content')

<div class="card">
    <div class="row mt-3 mb-5 mt-5">
        <div class="col-10 mx-auto">
            <label for="" class="h2 mx-3"> <b class="font-weight-bold">Noticias</b> / {{ $new->title }}</label>
        </div>
    </div>


    <div class="row-reverse">
        <div class="col-10 mx-auto">
            <div class="row-reverse">
                <div class="col d-flex justify-content-center mb-5 shadown">
                        <img width="600px" height="300" src="{{ $new->getFirstMediaUrl('banner') }}" alt="">
                </div>
                <div class="col">
                    <label for="" class="h4 font-weight-bold">{{ $new->title }}</label>
                </div>
                <div class="col">
                    <p class="text-dark">{{ $new->body }}</p>
                </div>
                <div class="col my-5">
                    <div class="row">
                        <div class="col">
                            <p class="h4"><strong>{{ 'Publicado por: ' . $new->author->name }}</strong></p>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <p class="h4"><strong>{{ 'Fecha de publicación: ' . $new->created_at }}</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <style>
        .image_notice {
            background-image: url({{ $new->getFirstMediaUrl('banner') }});
            background-size: 100% 100%;
            min-height:50vh;
            background-repeat: no-repeat;
            border-radius: 20px;
        }

    </style>
@endsection
