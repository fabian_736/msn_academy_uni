@extends('landing.layouts.patient.app')
@section('content')
<link rel="stylesheet" href="{{asset('landing/css/blog/post.css')}}">
<div class="row">
    <div class="col-10 mx-auto card card-body p-4">
        @if (session('message'))
<div class="alert alert-success" role="alert">
  <i class="fa fa-check mr-2"></i> Comentario creado
</div>
<br><br>
@endif
        <div class="row">
            <div class="col-4 d-flex justify-content-center">
                <img width="150px" height="150px" src="{{ url('landing/img/user/3.png') }}">
            </div>
            <div class="col">
                <div class="row-reverse">
                    <div class="col">
                        <label for="" class="h3 font-weight-bold">{{'Autor: '.$post->author->name}}</label>
                    </div>
                    <div class="col">
                        <p class="h4 font-weight-bold">{{$post->title}} </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 p-5">
            <div class="col-sm-8 text-center @if(strlen($post->body)<= 260) mt-5  @endif">
                <p class="h4 text-bold"><b>{{$post->body}}</b> </p>
            </div>
            <div class="col-sm-4">
                @if( pathinfo( $post->getFirstMediaUrl('file'), PATHINFO_EXTENSION) == 'mp4') 
                <video width="300px" height="300px" src="{{ $post->getFirstMediaUrl('file') }}" autoplay muted loop></video>
                @else
                <img width="100%" height="100%" src="{{ $post->getFirstMediaUrl('file') }}" alt="">
                @endif
            </div>
        </div>
    </div>
    <div class="col-10 mx-auto card card-body p-4">
        <div class="container mt-5 mb-5">
            <div class="row height d-flex justify-content-center align-items-center">
                <div class="col-md-10">
                    <div class="">
                        <div>
                            <div class="p-3">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <h2><b>Comentarios</b> <i class="fas fa-comment-alt ml-2"></i></h2>
                                    </div>
                                    <div class="col-sm-3">
                                        <a style="color:black !important;font-size:20px" href="#" data-id="{{$post->id}}" class="like-btn">
                                            <div id="like">
                                                <span class="iconify mr-1" data-icon="ant-design:like-outlined" data-width="20"></span>
                                                Me Gusta
                                            </div>
                                        </a>
                                        <a disabled style="color:black !important;font-size:20px" href="#" data-id="{{$post->id}}" class="dislike-btn">
                                            <div id="dislike" style="display:none">
                                                <span class="iconify mr-1" data-icon="ant-design:like-outlined" data-width="20"></span>
                                                Te Gusta
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <form action="{{route('blog_patient.store')}}" method="post" id="register_comment" enctype="multipart/form-data">
                                @csrf
                                <div class="mt-3  p-3 form-color">
                                    <input type="hidden" name="post_id" value="{{$post->id}}">
                                    <div class="input-group mb-3">
                                        <input autofocus required id="chatSend" name="comment" type="text" style="height:39px" class="form-control mt-1 mr-2" placeholder="Haz tu comentario...">
                                        <button class=" btn btn-success" style="height:35px !important;">Comentar</button>
                                    </div>
                                    <script>
                                        ClassicEditor
                                            .create(document.querySelector('#chatSend'))
                                            .then(editor => {
                                                console.log(editor);
                                            })
                                            .catch(error => {
                                                console.error(error);
                                            });
                                    </script>
                                </div>
                            </form>
                        </div>
                        <div class="mt-2" @if(count($post->comments)==0) @else style="overflow-y:auto;height:400px" @endif>
                           @if (count($post->comments) >=1)
                           @foreach($post->comments as $comment)
                           <div class="d-flex flex-row p-3">
                               <img src="{{ $comment->user->getFirstMediaUrl('avatar') }}" width="40" height="40" class="rounded-circle mr-3">
                               <div class="w-100">
                                   <div class="d-flex justify-content-between align-items-center">
                                       <div class="d-flex flex-row align-items-center"> <span class="mr-2">{{$comment->user->name}}</span>
                                           @if ($loop->first)<small class="c-badge">Nuevo</small> @endif
                                       </div>
                                       <small>{{ carbon\carbon::parse($comment->created_at)->diffforhumans() }}</small>
                                   </div>
                                   <p class="text-justify comment-text mb-0">{{$comment->comment}}</p>
                               </div>
                           </div>
                           @endforeach
                           @else
                           <div class="d-flex justify-content-center alert bg-dark text-white m-auto col-sm-6" role="alert">
                            <h4 class="text-center"><i class="fas fa-bell mr-2"></i> Se el primero en comentar está publicación</h4>
                           </div>
                           @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var user_id = '{{Auth::user()->id}}';
        var url = "{{route('blog.like',[$post])}}";
        var _token = "{{csrf_token()}}";
        var _like = "{{$i_like_post}}";


        if (_like) {
            $("#like").hide();
            $("#dislike").show();
        } else {
            $("#like").show();
            $("#dislike").hide();
        }

        $(document).ready(function() {

            $('.dislike-btn').on('click', function() {
                var action = 'dislike';
                var post_id = '{{$post->id}}';
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {
                        'action': action,
                        'post_id': post_id,
                        'user_id': user_id,
                        '_token': _token,
                    },
                    success: function(data) {
                        if (data.i_like_it == 1) {
                            $("#like").hide();
                            $("#dislike").show();
                        } else if (data.i_like_it == 0) {
                            $("#like").show();
                            $("#dislike").hide();
                        }
                    }
                });

            });


            $('.like-btn').on('click', function() {
                var action = 'like';
                var post_id = '{{$post->id}}';
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {
                        'action': action,
                        'post_id': post_id,
                        'user_id': user_id,
                        '_token': _token,
                    },
                    success: function(data) {
                        if (data.i_like_it == 1) {
                            $("#like").hide();
                            $("#dislike").show();
                        } else if (data.i_like_it == 0) {
                            $("#like").show();
                            $("#dislike").hide();
                        }
                    }
                });

            });

        });
    </script>
    @endsection