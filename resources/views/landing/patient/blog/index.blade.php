{{-- @dd($posts) --}}
@extends('landing.layouts.patient.app')
@section('content')

@if(count($posts)> 0)
@foreach($posts as $post)
<div class="row">
    <div class="col-10 mx-auto card card-body">
        <div class="row">
            <div class="col-4 d-flex justify-content-center">
                <img src="{{ url('landing/img/user/3.png') }}" alt="" style="min-height: 25vh ; max-height: 25vh">
            </div>
            <div class="col">
                <div class="row-reverse">
                    <div class="col">
                        <p class="h3 font-weight-bold">{{$post->title}} </p>
                    </div>
                    <div class="col">
                        <p class="h4">{{$post->body}}.</p>
                    </div>

                    <div class="col my-3">
                        <div class="row">
                            <div class="col d-flex justify-content-end align-items-center">
                                <a href="{{url('blog/post', [$post])}}" class="btn btn-primary"><span class="iconify" data-icon="akar-icons:arrow-forward-thick-fill" class="mr-2" style="color: white;"></span> Ver publicación </a>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <p class="h4 font-weight-bold"><strong>{{ 'Publicado por: '.$post->author->name }}</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
{{ $posts->links() }}
@else
<x-modal-alert message='Publicaciónes' />
@endif
@endsection