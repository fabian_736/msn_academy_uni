@extends('landing.layouts.patient.app')
@section('content')
<script src="{{asset('landing/js/cuestionario/index.js')}}"></script>
<link rel="stylesheet" href="{{asset('landing/css/cuestionario/index.css')}}">
<div class="row-reverse">
    <div class="col">
        <label for="" class="h2 font-weight-bold mx-3"><b>Cuestionario |</b> {{ $patientCourse->course->name }}</label>
    </div>
    <div class="col">
        <label for="" class="h3 font-weight-bold mx-3">Bienvenido a la prueba</label>
        <hr>
    </div>
</div>
<ul>
    @foreach ($errors->all() as $error)
    <div class="alert alert-warning" role="alert">
        <span>{{ $error}}</span>
    </div>
    @endforeach
</ul>


<div class="row my-5 mt-4 ">
    @if(count($patientCourse->course->questions) <=0) <div class="container text-center">
        <div class="alert alert-warning" role="alert">
            <h3><i class="fa fa-exclamation-triangle mr-3"></i> No hay preguntas registradas</h3>
        </div>
</div>
@else
<div class="col-sm-10 m-auto r-100" id="item1">
    <form onsubmit="return checkSubmit();" id="challenge-form" action="{{ route('modules.questionnaire.index',["patientTraining"=>$patientTraining->id, "patientCourse"=>$patientCourse->id]) }}" method="post">
        @csrf
        @foreach ($patientCourse->course->questions as $question )
        <div class="card p-4 d-block row-reverse">
            <div class="col p-0  my-3">
                <label for="" class="h4 font-weight-bold"><b>{{ $loop->iteration }}. ¿{{ $question->label }}?</b></label>
            </div>
            <div class="row mx-auto mb-2">
                @foreach ($question->answers as $answer)
                <div @if(in_array($answer->getFirstMedia('banner_answer')?->extension, ['jpg','png','jpeg'])) class="col-md-3 d-flex justify-content-center" @else class="col-md-12" @endif >
                    <div class="row mx-auto p-0">
                        <div class="col-md-1 p-0 mt-3 d-flex justify-content-center align-items-center">
                            <div class="list-group fileinput fileinput-new text-center m-0" data-provides="fileinput">
                                @if($question->type == 'unique')
                                <label class="container">
                                    <input required class="form-check-input" type="radio" name="answer_option_{{$question->id}}" id="" value="{{$answer->id}}">
                                    <span class="checkmark"></span>
                                </label>
                                @else
                                <label class="container">
                                    <input data-required class="form-check-input" type="checkbox" name="answer_option_{{$question->id}}[]" id="" value="{{$answer->id}}">
                                    <span class="checkmark"></span>
                                </label>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-10 p-0 mt-3">
                            <div class="row-reverse p-0 m-0 ">
                                <div class="col m-0 p-0">
                                    @if(in_array($answer->getFirstMedia('banner_answer')?->extension, ['jpg','png','jpeg']))
                                    <img style="border-radius:100%" width="200px" height="200px" src="{{ $answer->getFirstMediaUrl('banner_answer') }}" alt="">
                                    @elseif (in_array($answer->getFirstMedia('banner_answer')?->extension, ['ogg','mp3']))
                                    <div class="d-flex justify-content-center">
                                        <audio preload="auto" data-bs-toggle="tooltip" data-bs-placement="top" title="Audio de la respuesta" controls>
                                            <source src="{{$answer->getFirstMediaUrl('banner_answer')}}" type="audio/ogg">
                                            <source src="{{$answer->getFirstMediaUrl('banner_answer')}}" type="audio/mp3">
                                            Tu navegador no soporta audio HTML5.
                                        </audio>
                                    </div>
                                    @else
                                    <label for="" class="h5 font-weight-bold mt-4" style="float: left; color: #38B9C3"><b>{{
                                        $answer->label }}</b>
                                    </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        @endforeach
        @if (session()->has('status'))
        <script>
            $(document).ready(function() {
                $("#exampleModal").modal("show");
                window.location.href = "{{ route('index') }}";
            });
        </script>
        @endif
        <button type="submit" class="btn btn-primary mt-5 btn-block">Enviar</button>
    </form>
</div>
@endif
@endsection

