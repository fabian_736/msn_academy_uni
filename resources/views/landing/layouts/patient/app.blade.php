    <!-- head -->
    @include('landing.layouts.patient.components.head')
    <!-- end head -->


    <!-- Sidebar -->
    @include('landing.layouts.patient.components.sidebar')
    <!-- end Sidebar -->


    <div class="main-panel">

        <!-- Navbar -->
        @include('landing.layouts.patient.components.navbar')
        <!-- End Navbar -->

        <div class="content">
            <div class="content">
                <div class="container-fluid py-5">
                    @yield('content')
                </div>
            </div>
        </div>

       <!-- footer -->
        @include('landing.layouts.patient.components.footer')
        <!-- end footer --> 


    </div>
    </div>

    <!-- end html -->
    @include('landing.layouts.patient.components.end')
    <!-- end end-html -->
