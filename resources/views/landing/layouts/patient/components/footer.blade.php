<body>
       <!-- Modal -->
  <div class="modal fade modal-dialog-scrollable " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title mt-3" id="exampleModalLabel">
            Si desea informar o reportar un efecto adverso o un reclamo técnico de producto asociado a un producto MSN,
            por favor, póngase en contacto con su médico o profesional de la salud y/o dirija sus comentarios teniendo
            en cuenta la siguiente información:
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul>
              <li>
                <strong>Colombia y Centro América: </strong>  Si usted está en Colombia, Guatemala, Jamaica, Trinidad and Tobago, República Dominicana por favor contacte a su autoridad local, a cualquiera de las líneas de contacto del programa y/o envié un correo al departamento de asuntos regulatorios, <strong>correo: </strong> <a href="mailto:regulatory.colombia@msnlabs.com.">regulatory.colombia@msnlabs.com.</a>
              </li><br>
              <li>
                <strong>Perú: </strong>  Si usted está en Perú, por favor contacte a su autoridad local, a cualquiera de las líneas de contacto del programa y/o envié un correo al departamento de asuntos regulatorios, <strong>correo: </strong> <a href="mailto:msnlabsperu@gmail.com.">msnlabsperu@gmail.com.</a>
              </li><br>
              <li>
                <strong>Chile: </strong>  Si usted está en Perú, por favor contacte a su autoridad local, a cualquiera de las líneas de contacto del programa y/o envié un correo al departamento de asuntos regulatorios, <strong>correo: </strong> <a href="mailto:regulatory.chile@msnlabs.com.">regulatory.chile@msnlabs.com.</a>
              </li><br>
          </ul>
        </div>
      </div>
    </div>
  </div>
</body>
<footer class="footer fixed-bottom" style="background: #eee">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <span href="" class="h4 font-weight-bold footerSpan" style="">Contactar a Línea nacional: 018000114817</span>
                <span class="mx-3 font-weight-bold">|</span>
                <a href="" id="politic" class="h4 font-weight-bold footerSpan">Políticas de uso</a>
                <span class="mx-3 font-weight-bold">|</span>
                <a href="" id="politic" class="h4 font-weight-bold footerSpan" data-toggle="modal" data-target="#exampleModal">En caso de efecto adverso o queja de calidad</a>
            </div>
        </div>
    </div>
</footer>
<style>
    @media(max-width:800px){
        .footerSpan{
            font-size: 13px;
        }
    }
</style>
<script>
    $('#politic').click(function() {
        window.open('{{ url('pdf/politic.pdf') }}', 'dsfsdfsf',
            "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=4000,height=4000")
    })
</script>
