<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute  sticky-top p-4">
    <div class="container-fluid">
        <div class="navbar-wrapper">

            <div class="col">
                <form class="navbar-form">
                    <div class="input-group no-border">
                        <input type="text" value="" class="form-control" placeholder="Search..."
                            style="padding-right: 15vw">
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="col d-flex justify-content-end " id="row_profile">
            <div class="row ">
                <div class="col d-flex justify-content-end align-items-center pr-0 ">
                    <div class="row-reverse">
                        <div class="col text-right">
                            <span class="lead text-dark font-weight-bold">{{ auth()->user()->fullname }}</span>
                        </div>
                        <div class="col text-right">
                            <label for="" class="text-primary">{{ auth()->user()->patient->pap }}</label>
                        </div>
                    </div>
                </div>

                <div class="col d-flex justify-content-start p-0 ">
                    <div class="d-flex justify-content-center align-items-center "
                        style="width:80px; height: 80px; border-radius: 60px; background: #8246AF;">
                        <img src="{{ auth()->user()->getFirstMediaUrl('avatar') }}" alt=""
                            style="width:90%; height: 90%; border-radius: 60px;">
                        <a href="javascript:;" class=" d-flex justify-content-center align-items-center" role="button"
                            data-html="true" data-toggle="popover" data-trigger="focus" data-content="<div class='style2 container' style='max-height: 400px; overflow: auto'>


                        <!-- NOTIFICACIONES NUEVAS -->

                        <div class='row my-3'>
                            <div class='col'>
                                <label for='' style='color: #7DBE38'>Notificaciones nuevas</label>
                            </div>
                        </div>
                        <div class='row-reverse'>
                          <div class='col '>
                            <div class='row '>
                              <div class='col-2 p-0 d-flex justify-content-center align-items-center'>
                                <img src='https://electronicssoftware.net/wp-content/uploads/user.png' alt='' class='w-100 rounded-circle'>
                              </div>
                              <div class='col d-flex justify-content-center align-items-center mt-1'>
                                <label for='' class='text-dark' style='font-weight: bold;'>Hay un nuevo desafio, descubrelo.</label>
                              </div>
                            </div>
                          </div>
                          <div class='col p-0'>
                            <hr />
                          </div>
                          <div class='col '>
                            <div class='row '>
                              <div class='col-2 p-0 d-flex justify-content-center align-items-center'>
                                <img src='https://electronicssoftware.net/wp-content/uploads/user.png' alt='' class='w-100 rounded-circle'>
                              </div>
                              <div class='col d-flex justify-content-center align-items-center mt-1'>
                                <label for='' class='text-dark' style='font-weight: bold;'>Hay un nuevo desafio, descubrelo.</label>
                              </div>
                            </div>
                          </div>
                          <div class='col p-0'>
                            <hr />
                          </div>
                        </div>


                        <!-- NOTIFICACIONES ANTIGUAS -->

                        <div class='card p-3' style='background: #E5E5E5;'>
                          <div class='row my-3'>
                            <div class='col'>
                                <label for='' style='color: #8246AF'>Notificaciones antiguas</label>
                            </div>
                          </div>
                          <div class='row-reverse'>
                            <div class='col '>
                              <div class='row '>
                                <div class='col-2 p-0 d-flex justify-content-center align-items-center '>
                                  <img src='https://electronicssoftware.net/wp-content/uploads/user.png' alt='' class='w-100 rounded-circle'>
                                </div>
                                <div class='col d-flex justify-content-center align-items-center mt-1'>
                                  <label for='' class='font-weight-bold' >Hay un nuevo desafio, descubrelo.</label>
                                </div>
                              </div>
                            </div>
                            <div class='col p-0'>
                              <hr />
                            </div>
                            <div class='col '>
                              <div class='row '>
                                <div class='col-2 p-0 d-flex justify-content-center align-items-center'>
                                  <img src='https://electronicssoftware.net/wp-content/uploads/user.png' alt='' class='w-100 rounded-circle'>
                                </div>
                                <div class='col d-flex justify-content-center align-items-center mt-1'>
                                  <label for='' class='font-weight-bold'>Hay un nuevo desafio, descubrelo.</label>
                                </div>
                              </div>
                            </div>
                            <div class='col p-0'>
                              <hr />
                            </div>
                          </div>
                        </div>

                      </div>" id="example">
                            <div class="mt-5 mr-3 d-flex justify-content-center align-items-center"
                                style="position: absolute; background: #46C8E1; width: 25px; height: 25px; border-radius: 40px">
                                <i class="material-icons text-white " style="font-size: 20px">notifications</i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

<style>
      @media(max-width: 1057px) {
            #row_profile{
              display: none !important;
            }

        }
</style>
