<div class="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg" style="z-index: 10000 !important">
    <div class="logo d-flex justify-content-center ">
        <img src="{{ url('landing/img/logo/1.png') }}" alt="" id="img_initial_sidebar">
        <img src="{{ url('landing/img/logo/2.png') }}" alt="" id="img_initial_minisidebar" style="display: none">
    </div>
    <div class="navbar-wrapper mt-5" style="position: absolute; z-index: 100000; right: -20px; top:0">
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-just-icon btn-fab btn-round" style="background: #E30613">
                <i onclick="arrowleft_sidebar()" class="material-icons text_align-center visible-on-sidebar-regular">arrow_left</i>
                <i onclick="arrowright_sidebar()" class="material-icons design_bullet-list-67 visible-on-sidebar-mini">arrow_right</i>
            </button>

        </div>
    </div>

    <script>
        function arrowleft_sidebar() {
            $("#img_initial_minisidebar").show();
            $("#img_initial_sidebar").hide();

        }

        function arrowright_sidebar() {
            $("#img_initial_sidebar").show();
            $("#img_initial_minisidebar").hide();

        }
    </script>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item {{ request()->is('home') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/home') }}">
                    <i class="material-icons"><span class="iconify"
                            data-icon="ant-design:home-filled"></span></i>
                    <p> Inicio </p>
                </a>
            </li>
            <li class="nav-item {{ request()->is('perfil') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('perfil.index') }}">
                    <i class="material-icons"><span class="iconify" data-icon="fa6-solid:user"></span></i>
                    <p> Perfil </p>
                </a>
            </li>
            <li class="nav-item {{ request()->is('modules') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('modules.index')}}">
                    <i class="material-icons"><span class="iconify" data-icon="fluent:server-play-20-filled"></span></i>
                    <p> Mis cursos </p>
                </a>
            </li>
            <li class="nav-item {{ request()->is('contents/list_news_patient', 'new/details/*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('contents.list_news_patient') }}">
                    <i class="material-icons"><span class="iconify" data-icon="fluent:receipt-24-filled"></span></i>
                    <p> Noticias </p>
                </a>
            </li>
            <li class="nav-item {{ request()->is('blog', 'blog/post/*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('content.patient_post_list') }}">
                    <i class="material-icons"><span class="iconify" data-icon="ic:outline-message"></span></i>
                    <p> Hablemos para cuidarte </p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" onclick="document.getElementById('logout').submit()">
                    <i class="material-icons"><span class="iconify" data-icon="codicon:sign-out"></span></i>
                    <p> Cerrar Sesión </p>
                </a>
            </li>
        </ul>
    </div>
    <form id="logout" action="{{ route('logout') }}" method="post">
        @csrf
    </form>
</div>
