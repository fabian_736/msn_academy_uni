<div class="row" style="padding-top: 250px">
    <div class="col d-flex justify-content-end mr-3">
        <i class="fas fa-bell fa-lg"></i>
    </div>
</div>
<div class="row my-4">
    <div class="col d-flex justify-content-end mr-3">
        <i class="fab fa-instagram fa-lg"></i>
    </div>
</div>
<div class="row">
    <div class="col d-flex justify-content-end mr-3">
        <i class="fab fa-facebook fa-lg"></i>
    </div>
</div>
<div class="row my-4">
    <div class="col d-flex justify-content-end mr-3">
        <i class="fab fa-whatsapp fa-lg"></i>
    </div>
</div>
<div class="row  d-flex justify-content-end mr-1" style="margin-top: 120px">
    <label for="" style="writing-mode: vertical-lr;
transform: rotate(180deg);">COLOMBIA</label>
</div>