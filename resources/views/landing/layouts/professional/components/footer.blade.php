<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="https://www.peoplemarketing.com/">
                        CONTACTANOS
                    </a>
                </li>
                <li>
                    <a href="https://www.peoplemarketing.com/">
                        SOBRE NOSOTROS
                    </a>
                </li>
                <li>
                    <a href="https://www.peoplemarketing.com/">
                        AYUDA
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script> All reserved copyright by
            <a href="https://www.peoplemarketing.com/" target="_blank">PEOPLE MARKETING S.A.S</a>
        </div>
    </div>
</footer>