@extends('layouts.professional.app')
@section('content')

<div class="row-reverse p-0 m-0">
    <div class="col p-0 m-0">
        <label for="" class="h3 font-weight-bold" style="color: #38B9C3">Muro</label>
    </div>
    <div class="col p-0 m-0">
        <label for="" class="lead font-weight-bold" style="color: #38B9C3">Tema del mes</label>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card p-3 mx-auto">
            <div class="row">
                <div class="col-md-2 d-flex justify-content-center">
                    <div class="fileinput fileinput-new text-center " data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-circle img-raised p-1 bg-white" style="width:300px; height:100px">
                            <a href="{{route('perfil_professional.index')}}"><img src="{{ url('img/user2.png') }}" style="border-radius: 60px; width: 100%; height: 100%" rel="nofollow" alt="..."></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row d-flex justify-content-between">
                        <div class="col">
                            <label for="" class="lead font-weight-bold" style="color: #38B9C3">Dra. Laura Gomez</label>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <label for="" class="font-weight-bold" style="color: #38B9C3">Tema del mes</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation incidunt ut laoreet dolore magna aliquam erat volutpat. </label>
                        </div>
                    </div>
                    <div class="row mx-auto">
                        <div class="col-md-9 m-0 p-0">
                            <textarea id="chatSend" name="chat_message" cols="30" rows="10" class="form-control" placeholder="Escribe un comentario..." style="width: 100%; height: 40px !important; background: #F5F5F5; border-radius: 20px; padding: 10px"></textarea>
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 p-0 m-0 d-flex justify-content-center align-items-center" style="cursor: pointer; width: 40px; height: 40px; border-radius: 60px; background: #38B9C3">
                            <i class="fas fa-paper-plane" style="font-size: 20px; color: white; "></i>
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 pt-2 m-0 d-flex justify-content-center align-items-center">
                            <label for="file-upload" class="subir text-dark ">
                                <i class="fas fa-paperclip m-0 p-0" style="font-size: 20px; cursor: pointer;"></i>
                            </label>
                            <input id="file-upload" onchange='cambiar()' type="file" style='display: none;' />
                   
                        </div>
                        <div class="col-md-1 col-sm-12 col-xs-12 p-0 m-0 d-flex justify-content-start align-items-center" style="cursor: pointer;">
                        <i class="far fa-thumbs-up" style="color: #AEDC5A; font-size: 20px" ></i>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col">
                            <div id="chat_converse" class="chat_converse">
                                
                            </div>
                         
                        </div>
                    </div>
                       
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function cambiar() {
        var pdrs = document.getElementById('file-upload').files[0].name;
        $('#blah').show();
   
    }



</script>

@endsection