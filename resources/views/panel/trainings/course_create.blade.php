<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Crear cursos</h5>
                <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-3">
                <div class="row">
                    <div class="col">
                        <form action="{{route('courses.store')}}" method="post" id="register" enctype="multipart/form-data">
                            @csrf
                            <div class="row-reverse">
                                <div class="col mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <input type="text" name="name" id="" class="form-control" placeholder="Titulo del curso" required>
                                        </div>
                                        <div class="col">
                                            <input type="text" name="description" id="" class="form-control" placeholder="Informacion del curso" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col mb-3">
                                    <div class="row">

                                        <div class="col">
                                            <select name="training_id" id="" class="form-control" required>
                                                <option value="" disabled selected>Enlazar al modulo...</option>
                                                @foreach($trainings as $training)
                                                <option value="{{$training->id}}">{{$training->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="col mb-3">
                                    <div class="row">
                                        <div class="col">
                                            {{-- <label for="">Adjuntar documento</label>
                                            <input accept=".pdf" type="file" name="document" id="" class="form-control" required> --}}
                                             <label for="">URL</label>
                                            <input type="text" name="URL" id="" class="form-control" placeholder="Digite la URL del google drive" required>
                                      
                                        </div>
                                    </div>
                                </div>
                                <div class="col mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="">Adjuntar imagen</label>
                                            <input accept="image/*" type="file" name="cur_banner" id="cur_banner" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center ">
                <button data-bs-toggle="tooltip" data-bs-placement="top" title="Agregar curso" form="register" class="btn btn-primary">REGISTRAR</button>
            </div>
        </div>
    </div>
</div>