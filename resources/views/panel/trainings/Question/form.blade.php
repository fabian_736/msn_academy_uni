<input required type="hidden" name="course_id" id="course_id" value="{{$course->id}}">
<label for="type">Tipo de pregunta</label>
<select required name="type" id="type" class="form-control">
    <option selected disabled>Selecciona una opción...</option>
    <option value="unique">Unica</option>
    <option  value="multiple">Multiple</option>
</select>
<label for="label">Digita la pregunta</label>
<textarea placeholder="Digita la pregunta" placeholder="Registrar pregunta" required class="form-control" name="label" id="label" cols="30" rows="2"></textarea>
