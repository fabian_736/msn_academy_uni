<!-- Button trigger modal -->
@if (count($course->questions) >= 1)
<div class="row mb-5">
    <div class="col-sm-8">
        <h2>Mis preguntas</h2>
    </div>
    <div class="col-sm-4">        
        <button type="button" class="btn btn-primary d-inline-block mt-3" data-bs-toggle="modal" data-bs-target="#crearpregunta">
            Crear Pregunta
        </button>
    </div>
</div>
@endif
<!-- Modal -->
<div class="modal fade" id="crearpregunta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Agregar Pregunta</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('questions.questions.store') }}" method="post">
                    @csrf
                    @include('panel.trainings.Question.form')

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Agregar pregunta</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
