<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editarpregunta{{ $question->id }}">
    <i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="editarpregunta{{ $question->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Editar Pregunta</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ url('questions/questions/'.$question->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="row m-auto">
                        <div class="col-sm-12">
                            <input required type="hidden" name="course_id" id="course_id" value="{{$course->id}}">
                            <label for="type">Tipo de pregunta</label>
                            <select name="type" id="type" class="form-control">
                                <option @selected($question->type == 'unique') value="{{$question->type}}">{{$question->type}}</option>
                                <option @selected($question->type == 'multiple') value="multiple">Multiple</option>
                            </select>
                            <label for="label">Digita la pregunta</label>
                            <textarea placeholder="Digita la pregunta" required class="form-control" type="text" name="label" id="label" cols="30" rows="4">{{ $question->label }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
