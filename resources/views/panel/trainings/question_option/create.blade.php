<!-- Button trigger modal -->
<button  type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#crearrespuesta{{ $question->id }}">
    <i class="fa fa-question-circle"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="crearrespuesta{{ $question->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Administrador de respuestas</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('questions.answers.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @include('panel.trainings.question_option.form')
                    <button type="submit" class="btn btn-primary  mt-3">Crear Respuesta</button>
                </form>
                @if (count($question->answers) <=0 )
                 @else
                 <div class='table-responsive'>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Respuesta</th>
                                <th>Tipo de respuesta</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($question->answers as $answers)
                            <tr>
                                <td><b>{{ $loop->iteration }}.</b> </td>
                                <td>@if($answers->hasMedia('banner_answer')){{ $answers->getFirstMedia('banner_answer')->file_name}} @else {{$answers->label}} @endif</td>
                                @if ($answers->is_correct == 1)
                                <td>Correcta</td>
                                @else
                                <td>Incorrecta</td>
                                @endif
                                <td>@include('panel.trainings.question_option.edit')
                                    <form style="display: inline-block" action="{{ url('questions/answers/' . $answers->id) }}" method="post">
                                        @csrf @method('DELETE')
                                        <button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar respuesta" onclick="return confirm('¿Desea borrar esta respuesta?')" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
            @endif
        </div>
    </div>
</div>
</div>
