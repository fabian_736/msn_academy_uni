<button data-bs-toggle="modal" data-bs-target="#editarrespuesta{{ $answers->id }}" class="btn btn-primary"><b><i class="fa fa-edit"></i></b></button>
<!-- Modal -->
<div class="modal fade" id="editarrespuesta{{ $answers->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-dark ">
                <h5 class="modal-title text-white" id="exampleModalLabel">Editar Respuesta</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ url('questions/answers/'.$answers->id) }}" enctype="multipart/form-data" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        @if( $answers->hasMedia('banner_answer'))
                        <div class="col-sm-12">
                            <input type="hidden" name="answer_image" value="1">
                            <input type="hidden" name="question_id" value="{{ $question->id }}">
                            <div class="d-flex justify-content-center">
                                @if(in_array($answers->getFirstMedia('banner_answer')?->extension, ['jpg','png','jpeg']))
                                <img data-bs-toggle="tooltip" data-bs-placement="top" title="Imagen de la respuesta" style="border-radius: 100%" width="200px" height="200px" src="{{ $answers->getFirstMediaUrl('banner_answer') }}" alt="">
                                @elseif (in_array($answers->getFirstMedia('banner_answer')?->extension, ['ogg','mp3']))
                                <audio data-bs-toggle="tooltip" data-bs-placement="top" title="Audio de la respuesta" controls>
                                    <source src="{{$answers->getFirstMediaUrl('banner_answer')}}" type="audio/ogg">
                                    <source src="{{$answers->getFirstMediaUrl('banner_answer')}}" type="audio/mp3">
                                        Tu navegador no soporta audio HTML5.
                                </audio>
                                @else
                                @endif
                            </div>
                            <br><label for="label">Respuestas con imagen o audio</label>
                            <input accept="image/*,audio/*" type="file" class="form-control label2{{ $question->id }}" name="image" id="label2">
                        </div>
                        @else
                        <div class="col-sm-12">
                            <input required type="hidden" name="question_id" value="{{ $question->id }}">
                            <label for="label">Digita la respuesta</label>
                            <input value="{{ isset($answers->label) ? $answers->label : '' }}" placeholder="Digie la Respuesta"
                                class="form-control" type="text" name="label" id="label1{{ $question->id }}">
                        </div>
                        @endif
                    </div>
                    <div class="form-check mt-4">
                        @if (isset($answers->is_correct))
                            <input @if ($answers->is_correct == 1) checked @endif required class="form-check-input" type="radio"
                                name="is_correct" id="flexRadioDefault1" value="1">
                            <label class="form-check-label" for="is_correct">
                                Correcta
                            </label>
                            <br>
                            <input @if ($answers->is_correct == 0) checked @endif required value="0" class="form-check-input" type="radio"
                                name="is_correct" id="flexRadioDefault2" value="0">
                            <label class="form-check-label" for="is_correct">
                                Incorrecta
                            </label>
                        @else
                            <input required class="form-check-input" type="radio" name="is_correct" id="flexRadioDefault1" value="1">
                            <label class="form-check-label" for="is_correct">
                                Correcta
                            </label>
                            <br>
                            <input checked required value="0" class="form-check-input" type="radio" name="is_correct" id="flexRadioDefault2"
                                value="0">
                            <label class="form-check-label" for="is_correct">
                                Incorrecta
                            </label>
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Guardar cambios">Guardar cambios</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
