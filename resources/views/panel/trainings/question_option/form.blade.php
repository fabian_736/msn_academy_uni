<div class="row">
    <div class="col-sm-6">
        <input required type="hidden" name="question_id" value="{{ $question->id }}">
        <label for="">Digita la respuesta</label>
        <input value="{{ isset($answers->label) ? $answers->label : '' }}" placeholder="Digie la Respuesta"
            class="form-control" type="text" name="label" id="label1{{ $question->id }}">
    </div>
    <div class="col-sm-6">
        <div class="form-check mt-2">
            <br>
            <label for="">Respuestas con imagen o audio</label>
            <input data-bs-toggle="tooltip" data-bs-placement="top" title="Seleccione si su respuesta es una imagen o un audio"  value="1" name="answer_image" id="label2{{ $question->id }}"
                class="form-check-input  label2{{ $question->id }}" type="checkbox">
        </div>
    </div>
    <div class="col-sm-12 mb-4">
        <div class="file" style="display: none">
            <div class="card card-body">
                <input accept="image/*,audio/*" type="file" class="form-control label2{{ $question->id }}" name="image" id="label2">
            </div>
        </div>
    </div>

</div>
<div class="form-check">
    @if (isset($answers->is_correct))
        <input @if ($answers->is_correct == 1) checked @endif required class="form-check-input" type="radio"
            name="is_correct" id="flexRadioDefault1" value="1">
        <label class="form-check-label" for="is_correct">
            Correcta
        </label>
        <br>
        <input @if ($answers->is_correct == 0) checked @endif required value="0" class="form-check-input" type="radio"
            name="is_correct" id="flexRadioDefault2" value="0">
        <label class="form-check-label" for="is_correct">
            Incorrecta
        </label>
    @else
        <input required class="form-check-input" type="radio" name="is_correct" id="flexRadioDefault1" value="1">
        <label class="form-check-label" for="flexRadioDefault1">
            Correcta
        </label>
        <br>
        <input checked required value="0" class="form-check-input" type="radio" name="is_correct" id="flexRadioDefault2"
            value="0">
        <label class="form-check-label" for="flexRadioDefault2">
            Incorrecta
        </label>
    @endif
</div>
<script>
    $(document).ready(function() {
        $("#label1{{ $question->id }}").click(function() {
            $(".label2{{ $question->id }}").val("");
            $("#label2{{ $question->id }}").prop("checked", false);
        });

        $(".label2{{ $question->id }}").click(function() {
            check1 = document.getElementById("label2{{ $question->id }}");
            $("#label1{{ $question->id }}").val("");
            if (check1.checked) {
                $(".file").show();
            } else {
                $(".file").hide();
            }
        });
    });
</script>
