<div class="row-reverse">
        <div class="row">
            <div class="col mb-3">
                <label for="">Nombre</label>
                <input required type="text" name="name" id="" class="form-control" placeholder="Titulo del modulo" value="{{isset($training->name)? $training->name:''}}" required>
            </div>
            <div class="col mb-3">
                <label for="">Patología</label>
                <select required name="type" id="" class="form-control" required>
                    <option value="" disabled selected>Tipo</option>
                    <option value="General" {{ ($training->type == 'General') ? "selected" : "" }}>General</option>
                    <option value="General" {{ ($training->type == 'Pathology') ? "selected" : "" }}>Patologia</option>
                </select>
            </div>
        </div>
       
        <div class="col mb-3">
            <div class="row">
                <div class="col">
                    <label for="">Descripción</label>
                    <textarea required name="description" id="" cols="10" rows="2" class="form-control" placeholder="Descripcion del modulo" required>{{$training->description}}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col mb-3">
                <div class="row">
                    <div class="col">
                        <label for="">Adjuntar documento de introducción</label>
                        <input accept=".pdf" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar documento" type="file" name="document" id="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <label for="">Adjuntar Imagen del modulo</label>
                        <input accept="image/*" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar imagen" type="file" name="mod_banner" id="mod_banner" class="form-control">
            </div>
        </div>
        <div class="modal-footer d-flex justify-content-center ">
            <button data-bs-toggle="tooltip" data-bs-placement="top" title="Actualizar modulo" type="submit" class="btn w-30 bg-gradient-primary">ACTUALIZAR</button>
        </div>
    </div>