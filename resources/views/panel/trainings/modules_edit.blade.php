@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col  card-body p-5">
        <div class="col my-5">
            <div class="row">
                <div class="col">
                    <label for="" class="h3">Editar Modulo</label>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

        <div class="row d-flex justify-content-center  g-4">
            <div class="col-8">
                <div class="card">
                    <img data-bs-toggle="tooltip" data-bs-placement="top" title="Imagen del modulo" src="{{$training->getFirstMediaUrl('banner')}}" class="card-img-top" alt="...">

                    <div class="card-body">
                        <h5 class="card-title">{{$training->name}}</h5>
                        <form action="{{route('trainings.update', [$training])}}" method="post" enctype="multipart/form-data" required>
                            @csrf
                            @include('panel.trainings.modules-form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection