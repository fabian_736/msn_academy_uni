    @extends('panel.layouts.app')
    @section('content')
    <div class="row-reverse">
        <div class="col card-body">
            @if (session('editada'))
            <div class="alert alert-success text-white alert-dismissible fade show" role="alert">
                <i class="fa fa-check mr-2"></i> Pregunta editada correctamente
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            @if (session('status'))
            <div class="alert alert-success text-white alert-dismissible fade show" role="alert">
                <i class="fa fa-check mr-2"></i> Pregunta creada correctamente
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            @if (session('eliminada'))
            <div class="alert alert-success text-white alert-dismissible fade show" role="alert">
                <i class="fa fa-check mr-2"></i> Pregunta eliminada correctamente
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            @if (session('message'))
            <div class="alert alert-success text-white alert-dismissible fade show" role="alert">
                <i class="fa fa-check mr-2"></i> Respuesta editada correctamente
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            <div class="col my-5">
                <div class="row">
                    <div class="col">
                        <label for="" class="h3">Editar Curso</label>
                    </div>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger text-white">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            <div class="row g-4 d-flex justify-content-center">
                <div class="col-8 ">
                    <div class="card">
                        <img data-bs-toggle="tooltip" data-bs-placement="top" title="Imagen del curso" src="{{$course->getFirstMediaUrl('banner')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title"> {{ $course->name }}</h5>
                            <form action="{{ route('courses.update', ['course' => $course]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row-reverse">
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Nombre del curso</label>
                                                <input type="text" name="name" id="" class="form-control" placeholder="Titulo del curso" value="{{ $course->name }}">
                                            </div>
                                            <div class="col">
                                                <label for="">Descripción</label>
                                                <input type="text" name="description" id="" class="form-control" placeholder="Informacion del curso" value="{{ $course->description }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Modulo</label>
                                                @if ($course->status == 'Publicado')
                                                <input disabled class="form-control" type="text" value="{{$course->training->name}}">
                                                <input type="hidden" name="training_id" value="{{$course->training->id}}">
                                                @else
                                                <select name="training_id" id="" class="form-control" required>
                                                    <option value="" disabled selected>Enlazar al modulo...</option>
                                                    @foreach ($trainings as $training)
                                                    <option value="{{ $training->id }}" {{ $training->id == $course->training_id ? 'selected' : '' }}>
                                                        {{ $training->name }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                                @endif
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="">Adjuntar URL</label>
                                                <input placeholder="Digite la url del google drive" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar la url" type="text" name="URL" id="" class="form-control" value="{{$course->URL}}">
                                                {{-- <input accept=".pdf" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar archivo" type="file" name="document" id="" class="form-control"> --}}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="cur_banner">Imagen del curso</label>
                                            <input accept="Image/*" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar imagen" type="file" name="ban" class="form-control" id="ban">
                                        </div>
                                        <div class="col-sm-6 float-right">
                                            @if ($course->status == 'Publicado')
                                            <input type="hidden" name="status" value="Publicado">
                                            <div class="alert alert-success text-white" role="alert">
                                                <b><i class="fa fa-check mr-3"></i> EL curso esta publicado</b>
                                            </div>
                                            @else
                                            <div class="wrapper">
                                                <input type="hidden" name="status" value="Borrador">
                                                <input type="radio" name="status" onchange="this.form.submit()" id="option-2" value="Publicado">
                                                <label data-bs-toggle="tooltip" data-bs-placement="top" title="Publicar el curso" for="option-2" class="option option-2">
                                                    <div class="dot"></div>
                                                    <span>Publicar</span>
                                                </label>
                                                <div class="publicado">
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer d-flex justify-content-center mt-5">
                                    <button data-bs-toggle="tooltip" data-bs-placement="top" title="Actualizar curso" type="submit" class="btn  bg-gradient-primary">ACTUALIZAR</button>
                                    @if ($course->questions->count() ==0)
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#crearpregunta">
                                        Crear Preguntaa
                                    </button>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        @can('delete',$course)
        <div class="row g-4 d-flex justify-content-center">
            <div class="col-8 ">
                <div class="card p-4"><br>
                    @include('panel.trainings.Question.create')
                    @if ($course->questions->count() >0)
                    <div class="col">
                        <div style="overflow-x: hidden" class="table-responsive">
                            <table class="table table-sm table-striped m-auto">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Pregunta</th>
                                        <th>Tipo</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($course->questions as $question)
                                    <tr>
                                        <td><b>{{ $loop->iteration }}.</b> </td>
                                        <td>{{Str::limit($question->label,10) }}</td>
                                        <td>{{ $question->type }}</td>
                                        <td>
                                            @include('panel.trainings.question_option.create')
                                            @include('panel.trainings.Question.edit')
                                            <form style="display: inline-block" action="{{ url('questions/questions/' . $question->id) }}" method="post">
                                                @method('DELETE')
                                                <button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar pregunta" onclick="return confirm('¿Desea Borrar esta Pregunta?')" type="submit" class="btn btn-danger"><i class="fa fa-trash text-white"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                    <div class="alert alert-primary text-white text-center" role="alert">
                        <i class="fa fa-exclamation-triangle mr-3"></i> Aun no hay respuestas en este curso
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @endcan
        <link rel="stylesheet" href="{{asset('landing/css/question_option/course_edit.css')}}">
        @endsection