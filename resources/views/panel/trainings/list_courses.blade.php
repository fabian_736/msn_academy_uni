@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col my-5">
        @include('panel.trainings.course_create')
        <div class="row">
            <div class="col">
                <label for="" class="h3">Cursos</label>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="javascript:;" class="btn btn-primary" title="Agregar Modulo" data-bs-toggle="modal" data-bs-target="#exampleModal2">Agregar
                    Curso</a>
            </div>
            @if(session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
            @endif
            @if(session('message'))
            <div class="alert alert-info">{{session('message')}}</div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
    <div class="col">
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">Titulo</th>
                    <th scope="col">Detalles</th>
                    <th scope="col">Modulo</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($courses as $course)
                <tr>
                    <td>{{$course->name}}</td>
                    <td>{{Str::limit($course->description,30)}}</td>
                    <td>{{$course->training_id}}</td>
                    <td> @if(count($course->media)>0)
                        {{$course->media->last()->name}}
                        @else
                        N/A
                        @endif
                    </td>
                    @can('delete',$course)
                    <td>
                        <button disabled class="btn btn-danger"><i class="fa fa-exclamation-triangle"></i> No Publicado</button>
                    </td>
                    @else
                    <td>
                        <button  disabled class="btn btn-success"><i class="fa fa-check-circle"></i> Publicado</button>
                    </td>
                    @endcan
                    <td >
                        <div class="row ">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle d-inline-block" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                 Acciones
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                  <li><a data-bs-toggle="tooltip" data-bs-placement="top" title="Editar curso" class="dropdown-item" href="{{route('courses.edit',[$course])}}"><i class="fas fa-edit"></i> Editar</a></li>
                                  <li>
                                    @can('delete',$course)
                                    <form class="d-inline-block" action="{{route('courses.destroy',[$course])}}" method="post">
                                        @csrf
                                        <button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar curso" class="dropdown-item" onclick="return confirm('¿Desea eliminar este curso?')" type="submit"><i class="fas fa-trash"></i> Borrar</button>
                                    </form>
                                @endcan
                                  </li>
                                </ul>
                              </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
