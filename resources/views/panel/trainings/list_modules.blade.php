@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col my-5">
        <div class="row">
            <div class="col">
                <label for="" class="h3">Modulos</label>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="javascript:;" class="btn btn-primary" title="Agregar Modulo" data-bs-toggle="modal" data-bs-target="#exampleModal">Agregar
                    Modulo</a>
            </div>
            @if(session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
            @endif
            @if(session('message'))
            <div class="alert alert-info">{{session('message')}}</div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
    <div class="col table-responsive">
        <table id="example" class="table table-sm  table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">Titulo</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Fecha de creación</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($trainings as $training)
                <tr>
                    <td>{{$training->name}}</td>
                    <td>{{Str::limit($training->description, 40)}}</td>
                    <td>{{$training->created_at}}</td>
                    <td>
                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="Editar modulo" href="{{route('trainings.edit',[$training])}}" class="btn btn-primary "><i class="fas fa-edit"></i></a>
                        <form class="d-inline-block" action="{{route('trainings.destroy',[$training])}}" method="post">
                            @csrf
                            <button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar modulo" onclick="return confirm('¿Desea eliminar este modulo?')" type="submit" class="btn btn-danger d-inline-block"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Crear modulos</h5>
                <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-3">
                <div class="row">
                    <div class="col">
                        <form action="{{route('trainings.store')}}" method="post" id="register" enctype="multipart/form-data" required>
                            @csrf
                            <div class="row-reverse">
                                <div class="row">
                                    <div class="col mb-3">
                                        <label for="">Nombre</label>
                                        <input required type="text" name="name" id="" class="form-control" placeholder="Titulo del modulo" required>
                                    </div>
                                    <div class="col mb-3">
                                        <label for="">Patología</label>
                                        <select required name="type" id="" class="form-control" required>
                                            <option value="" disabled selected>Tipo</option>
                                            <option value="General">General</option>
                                            <option value="General">Patologia</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <label for="">Descripción</label>
                                            <textarea required name="description" id="" cols="10" rows="2" class="form-control" placeholder="Descripcion del modulo" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col mb-3">
                                        <div class="row">
                                            <div class="col">
                                                <label for="">Adjuntar documento de introducción</label>
                                                <input accept=".pdf" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar documento" required type="file" name="document" id="" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="">Adjuntar Imagen del modulo</label>
                                        <input accept="image/*" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar imagen" required type="file" name="mod_banner" id="mod_banner" class="form-control">
                                    </div>
                                </div>
                                <div class="modal-footer d-flex justify-content-center ">
                                    <button data-bs-toggle="tooltip" data-bs-placement="top" title="Crear modulo" type="submit" class="btn w-30 bg-gradient-primary">Agregar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection