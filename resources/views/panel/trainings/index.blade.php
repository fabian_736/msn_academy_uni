{{-- @extends('panel.layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<div class="row">
    <div class="col">
        <div class="card card-body">
            <div class="row-reverse">
                <div class="col d-flex justify-content-center my-3">
                    <div class="row">
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="h3">Modulos</label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <a href="javascript:;" style="cursor:pointer" title="Agregar Modulo" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="fas fa-plus-circle fa-2x mb-1" style="color: #312783 "></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-center ">
                        <a href="{{ route('trainings.list_modules') }}" class="h5" style="text-decoration-line: underline">Ver Mas</a>
                    </div>
                </div>
                <div class="col ">
                    <div class="row-reverse ">
                        @foreach($trainings as $training)
                        <div class="col-10 mx-auto card_list mb-3">
                            <div class="card ">
                                <div class="img_card p-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row-reverse">
                                                <div class="col">

                                                    <p>{{$training->name}}</p>
                                                </div>
                                                <div class="col">
                                                    <p class="fa-sm">
                                                        {{$training->description}}
                                                    </p>
                                                </div>
                                                <div class="col">
                                                    <p>Numero de cursos: {{$training->courses_count}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card card-body">
            <div class="row-reverse">
                <div class="col d-flex justify-content-center my-3">
                    <div class="row">
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="h3">Cursos</label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <a href="javascript:;" style="cursor:pointer" title="Agregar Modulo" data-bs-toggle="modal" data-bs-target="#exampleModal2">
                                @include('panel.trainings.course_create')
                                <i class="fas fa-plus-circle fa-2x mb-1" style="color: #312783 "></i>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col d-flex justify-content-center ">
                        <a href="{{ route('trainings.list_courses') }}" class="h5" style="text-decoration-line: underline">Ver Mas</a>
                    </div>
                </div>
                <div class="col ">
                    <div class="row-reverse ">
                        @foreach($courses as $course)
                        <div class="col-10 mx-auto card_list mb-3 d-flex">
                            <div class="card ">
                                <div class="img_card p-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row-reverse">
                                                <div class="col">
                                                    <p>{{$course->name}}</p>
                                                </div>
                                                <div class="col">
                                                    <p class="fa-sm">{{$course->description}}
                                                    </p>
                                                </div>
                                                <div class="col">
                                                    <p>Modulo: {{ $course->training->name }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .card_list {
        min-height: 20vh;
        max-height: 20vh;
        background: #F5F5F5;
        border-radius: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .img_card {
        min-height: 15vh;
        max-height: 15vh;
        max-width: 30vw;
        min-width: 30vw;
        border-radius: 20px;
        cursor: pointer;
        overflow: hidden;
    }

    .img_card:hover {
        border-color: #312783;
        border-style: solid;
        border-width: 2px;
    }

    .img_curso {
        width: 100px;
        height: 100px;
        background: gray;
        border-radius: 60px;
    }
</style>
@endsection --}}
