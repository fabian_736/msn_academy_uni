@extends('panel.layouts.app')
@section('content')
@if (session('perfil_editado'))
<script>
    alert('perfil editado');
</script>
@endif
<div class="container-fluid">
    <div class="page-header min-height-150 border-radius-xl mt-4" style="background-image: url('img/login.png'); background-position-y: 50%;">
        <span class="mask bg-gradient-primary opacity-6"></span>
    </div>
    <div class="card card-body blur shadow-blur mx-4 mt-n6 overflow-hidden">
        <div class="row gx-4">
            <div class="col-auto">
                <div class="avatar avatar-xl position-relative">
                    <img src="{{Auth::user()->getFirstMediaUrl('avatar')}}" alt="" class="w-100 border-radius-lg shadow-sm">
                </div>
            </div>
            <div class="col-auto my-auto">
                <div class="h-100">
                    <h5 class="mb-1">
                        {{ Auth::user()->name }} {{ Auth::user()->surname }}
                    </h5>
                    <p class="mb-0 font-weight-bold text-sm">
                        {{ Auth::user()->user_type }}
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
                <div class="nav-wrapper position-relative end-0">
                    <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
                        <li class="nav-item">
                            <img src="{{ url('img/logo/1.png') }}" alt="" class="w-50">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12 col-xl-6 P-3">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-8 d-flex align-items-center">
                            <h6 class="mb-0">Informacion del perfil</h6>
                        </div>
                        <div class="col-md-4 text-end">
                            <a data-bs-toggle="tooltip" data-bs-placement="top" title="Editar el perfil" href="#" onclick="edit()" class="edit">
                                <i class="fas fa-user-edit text-secondary text-sm" title="Edit Profile"></i>
                            </a>
                            <a data-bs-toggle="tooltip" data-bs-placement="top" title="Guardar cambios" href="" onclick="confirm()" style="display: none" class="confirm">
                                <i class="fas fa-check-circle text-secondary text-md" title="Confirm"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body p-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>

                    <form action="{{ route('user-profile-information.update') }}" method="post">
                        @csrf
                        @method('PUT')
                        <ul class="list-group">
                            <li class="list-group-item border-0 ps-0 pt-0 text-sm">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-center">
                                        <strong class="text-dark">Nombre</strong>
                                    </div>
                                    <div class="col mt-3">
                                        <p id="print-2"> {{ Auth::user()->name }} {{ Auth::user()->surname }}</p>
                                        <input required type="text" name="name" id="input-2" class="form-control" placeholder="Nombre completo" value="{{ Auth::user()->name }}" style="display: none">
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item border-0 ps-0 text-sm">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-center">
                                        <strong class="text-dark">Apellido:</strong>
                                    </div>
                                    <div class="col mt-3">
                                        <p id="print-3">
                                            {{ Auth::user()->surname }}
                                        </p>
                                        <input required type="text" name="surname" id="input-3" class="form-control" placeholder="Telefono" value="{{ Auth::user()->surname }}" style="display: none">
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item border-0 ps-0 text-sm">
                                <div class="row">
                                    <div class="col-2 d-flex justify-content-end align-items-center">
                                        <strong class="text-dark">Correo:</strong>
                                    </div>
                                    <div class="col mt-3">
                                        <p id="print-4"> {{ Auth::user()->email }} </p>
                                        <input required type="email" name="email" value="{{ Auth::user()->email }}" id="input-4" class="form-control" placeholder="Correo electronico" style="display: none">
                                    </div>
                                </div>
                            </li>

                            <li class="bottom_update" style="display: none">
                                <div class="row">
                                    <div class="col d-flex justify-content-center">
                                        <button data-bs-toggle="tooltip" data-bs-placement="top" title="Guardar cambios" type="submit" class="btn btn-primary">ACTUALIZAR</button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 col-xl-6">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-md-8 d-flex align-items-center">
                            <h6 class="mb-0">Cambiar Contraseña</h6>
                        </div>
                        <div class="col-md-4 text-end">

                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('user-password.update') }}" method="post">
                        @csrf
                        @method('PUT')
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            Usuario modificado con éxito
                        </div>
                        @endif
                        <div class="card-body p-3">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>

                            <form action="{{ route('admin.profile.update') }}" method="post">
                                @csrf
                                @method('put')
                                <ul class="list-group">
                                    <li class="list-group-item border-0 ps-0 pt-0 text-sm">
                                        <div class="row">
                                            <div class="col-2 d-flex justify-content-end align-items-center">
                                                <strong class="text-dark">Nombre</strong>
                                            </div>
                                            <div class="col mt-3">
                                                <p id="print-2"> {{ Auth::user()->name }} {{ Auth::user()->surname }}
                                                </p>
                                                <input required type="text" name="name" id="input-2" class="form-control" placeholder="Nombre completo" value="{{ Auth::user()->name }}" style="display: none">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item border-0 ps-0 text-sm">
                                        <div class="row">
                                            <div class="col-2 d-flex justify-content-end align-items-center">
                                                <strong class="text-dark">Digita tu contraseña</strong>
                                            </div>
                                            <div class="col mt-3">
                                                <p id="print-1">Contraseña </p>
                                                <input required type="password" name="current_password" value="" id="input-1" class="form-control @error('current_password') is-invalid @enderror" placeholder="Contraseña" style="display: none">
                                            </div>
                                        </div>
                                    </li>

                                    <li class="list-group-item border-0 ps-0 text-sm">
                                        <div class="row">
                                            <div class="col-2 d-flex justify-content-end align-items-center">
                                                <strong class="text-dark">Nueva Contraseña:</strong>
                                            </div>
                                            <div class="col mt-3">
                                                <p id="print-8">Contraseña </p>
                                                <input required type="password" name="password" value="" id="input-8" class="form-control @error('password') is-invalid @enderror" placeholder="Contraseña" style="display: none">
                                            </div>
                                        </div>
                                    </li>

                                    <li class="list-group-item border-0 ps-0 text-sm">
                                        <div class="row">
                                            <div class="col-2 d-flex justify-content-end align-items-center">
                                                <strong class="text-dark">Repetir Contraseña:</strong>
                                            </div>
                                            <div class="col mt-3">
                                                <p id="print-5">Confirmar Contraseña </p>
                                                <input required type="password" name="password_confirmation" value="" id="input-5" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confirmar Contraseña" style="display: none">
                                            </div>
                                        </div>
                                    </li>
                                    <li class="bottom_update" style="display: none">
                                        <div class="row">
                                            <div class="col d-flex justify-content-center">
                                                <button data-bs-toggle="tooltip" data-bs-placement="top" title="Actualizar contraseña" type="submit" class="btn btn-primary">Actualizar
                                                    Contraseña</button>
                                            </div>
                                        </div>
                                    </li>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('landing/js/profile_admin/index.js')}}"></script>
    @endsection