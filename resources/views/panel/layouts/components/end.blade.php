<script src="{{ url('panel/js/app.js') }}"></script>

<script src="{{url('panel/datatable/jquery/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{url('panel/datatable/datatables/datatables.min.js')}}"></script>
<script src="{{url('panel/datatable/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('panel/datatable/datatables/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{url('panel/datatable/datatables/pdfmake-0.1.36/pdfmake.min.js')}}"></script>
<script src="{{url('panel/datatable/datatables/pdfmake-0.1.36/vfs_fonts.js')}}"></script>
<script src="{{url('panel/datatable/datatables/Buttons-1.5.6/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{url('panel/datatable/main.js')}}"></script>
<script src="{{asset('landing/js/validar-archivos.js')}}"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<style>
select{
    cursor: pointer;
}
</style>
<script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
</script>
<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
</body>

</html>
