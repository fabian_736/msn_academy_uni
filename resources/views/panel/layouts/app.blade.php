@include('panel.layouts.components.head')

@include('panel.layouts.components.sidebar')

<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">

  @include('panel.layouts.components.navbar')

  <div class="container-fluid py-4">

    @yield('content')

    {{-- @include('layouts.components.footer') --}}


  </div>

</main>

{{-- @include('layouts.components.paint') --}}

@include('panel.layouts.components.end')
<script>
  $(document).ready(function() {
    setTimeout(function() {
      $(".message").slideUp(500).empty();
      $(".alert").slideUp(500).empty();
      $(".success").slideUp(500).empty();
    }, 2000);

  });
</script>
