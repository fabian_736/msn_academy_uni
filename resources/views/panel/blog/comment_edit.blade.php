<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#user{{$comment->id}}">
  <i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="user{{$comment->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-dark">
        <h5 class="modal-title text-white" id="exampleModalLabel">Comentario de {{$comment->user->name}}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('blog.comments.post',$comment->id)}}" method="post">
          @csrf
          @method('PATCH')
          <div>
            <label for="exampleInputPassword1" class="form-label">Estado de la publicación</label>
            <select class="form-control" name="active" id="active">
              @if ($comment->active == 1)
              <option selected value="1">Activo</option>
              <option value="0">Inactivo</option>
              @else
              <option selected value="0">Inactivo</option>
              <option value="1">Activo</option>
              @endif
            </select>
          </div>
          <div>
            <label for="exampleInputEmail1" class="form-label">Responsable</label>
            <input value="{{$comment->user->name}}" disabled type="text" class="form-control" placeholder="Nombre del autor">
            <input value="{{$comment->user->id}}" type="hidden" name="user_id" id="user_id">
          </div>
          <div>
            <label for="exampleInputPassword1" class="form-label">Fecha de publicación</label>
            <input disabled value="{{$comment->created_at->format('Y-m-d')}}" type="date" class="form-control" id="exampleInputPassword1">
          </div>
          <div>
            <label for="exampleInputPassword1" class="form-label">Post</label>
            <textarea disabled class="form-control" cols="30" rows="3">{{$comment->post->title}}</textarea>
            <input type="hidden" name="post_id" value="{{$comment->post->id}}">
          </div>
          <div>
            <label for="exampleInputPassword1" class="form-label">Comentario</label>
            <textarea disabled class="form-control" name="comment" id="comment" cols="30" rows="3">{{$comment->comment}}</textarea>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar Cambios</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>