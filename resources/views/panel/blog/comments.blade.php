@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col my-5">
        <div class="row">
            <div class="col">
                <label for="" class="h3">Comentarios</label>
            </div>
        </div>
    </div>
    <div class="col">
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">Responsable</th>
                    <th scope="col">Fecha de creacion</th>
                    <th scope="col">Publicación</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($comments as $comment)
                <tr>
                    <td>{{$comment->user->name}}</td>
                    <td>{{$comment->created_at}}</td>
                    <td>{{$comment->post->title}}</td>
                    @if ($comment->active == 1)
                        <td>Activo</td>
                        @else
                        <td>Inactivo</td>
                    @endif
                    <td>
                         @include('panel.blog.comment_edit')
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>
@endsection
