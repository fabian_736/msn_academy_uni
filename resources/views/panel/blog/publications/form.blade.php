<div class="row">
    <div class="col-sm-6 ">
        <label for="">Titulo</label>
        <input required value="{{ isset($post->title) ? $post->title : '' }}" name="title"
            class="form-control" type="text" placeholder="Titulo">
    </div>
    <div class="col-sm-6 ">
        <label for="">Imagen o video</label>
        <input accept="image/*" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar imagen" required  name="file" id="file"
            class="form-control" type="file" placeholder="Titulo">
    </div>
    <div class="col-sm-12 ">
        <label for="">Descripción</label>
        <textarea class="form-control" required name="body" placeholder="Digite el nombre de la publicación..." cols="5"rows="2">{{ isset($post->body) ? $post->body : '' }}</textarea>
    </div>
    <div class="col-sm-6 ">
        <label class="form-label" for="">Fecha de publicacion</label>
        <input min="{{now()->format('Y-m-d')}}" required value="{{ isset($post->publication_starts_at) ? $post->publication_starts_at->format('Y-m-d') : '' }}"
            name="publication_starts_at" class="form-control" type="date" placeholder="Titulo">
    </div>
    <div class="col-sm-6 ">
        <label class="form-label" for="">Fecha de caducidad</label>
        <input min="{{now()->format('Y-m-d')}}" required value="{{ isset($post->publication_ends_at) ? $post->publication_ends_at->format('Y-m-d') : '' }}"
            name="publication_ends_at" class="form-control" type="date" placeholder="Titulo">
    </div>
</div>
<script>
    CKEDITOR.replace( 'id{{isset($post->id) ? $post->id : ''}}');
</script>
