@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col my-5">
        <div class="row">
            <div class="col">
                <label for="" class="h3">Publicaciones</label>
            </div>
            @if(session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
            @endif
            @if(session('message'))
            <div class="alert alert-success">{{session('message')}}</div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @include('panel.blog.publications.create')
        </div>

    </div>
    <div class="col">
     <div class="table-responsive">
        <table id="example" class="table-sm table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">Titulo</th>
                    <th scope="col">Contenido</th>
                    <th scope="col">Autor</th>
                    <th scope="col">Fecha de publicacion</th>
                    <th scope="col">Fecha de caducidad</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                <tr>
                    <td>{{Str::limit($post->title,10)}}</td>
                    <td>{{Str::limit($post->body,15)}}</td>
                    <td>{{$post->author->name}}</td>
                    <td>{{$post->publication_starts_at->format('d/m/Y')}}</td>
                    <td>{{$post->publication_ends_at->format('d/m/Y')}}</td>
                    <td>
                        <form class="d-inline-block" action="{{route('blog.publications.destroy',[$post])}}" method="post">
                            @csrf
                            <button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar publicación" onclick="return confirm('¿Desea Eliminar Está Publicación?')" type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                          </form>
                        @include('panel.blog.publications.edit')
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
     </div>
    </div>
</div>
@endsection