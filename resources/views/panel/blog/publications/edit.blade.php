<!-- Button trigger modal -->

  <button type="button" class="btn btn-primary d-inline-block" data-bs-toggle="modal" data-bs-target="#editar{{ $post->id }}">
    <i class="fa fa-edit"></i>
  </button>
<!-- Modal -->
<div class="modal fade" id="editar{{ $post->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-dark">
        <h5 class="modal-title text-white" id="exampleModalLabel">Editar Publicacion</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="">
        <div class="row g-4">
          <div class="col-12">
            <div class="card">
              <img data-bs-toggle="tooltip" data-bs-placement="top" title="Imagen de la publicación" src="{{$post->getFirstMediaUrl('file')}}" class="" alt="...">
              <div class="card-body">
                <h5 class="card-title mb-3">{{$post->title}}</h5>
                <form action="{{ route('blog.publications.update',[$post]) }}" method="post" enctype="multipart/form-data">
                  @csrf
                  @include('panel.blog.publications.form')
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
