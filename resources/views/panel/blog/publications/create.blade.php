<button data-bs-toggle="modal" data-bs-target="#crearpublicacion" class="btn btn-primary col-2">Crear Publicacion</button>
<!-- Modal -->
<div class="modal fade" id="crearpublicacion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Crear Publicacion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <form action="{{route('blog.publications.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @include('panel.blog.publications.form')
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Crear Publicacion</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
