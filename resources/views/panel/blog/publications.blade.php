@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col my-5">
        <div class="row">
            <div class="col">
                <label for="" class="h3">Publicaciones</label>
            </div>
        </div>
    </div>
    <div class="col">
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">Responsable</th>
                    <th scope="col">Titulo</th>
                    <th scope="col">Fecha publicación</th>
                    <th scope="col">Fecha finalización</th>
                    <th scope="col">Orden</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Fecha de creacion</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($posts as $post)
                <tr>
                    <td>{{$post->author->name}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->publication_starts_at}}</td>
                    <td>{{$post->publication_ends_at}}</td>
                    <td>{{$post->order}}</td>
                    <td>{{$post->active}}</td>
                    <td>{{$post->created_at}}</td>
                    <td>
                        <div class="row ">
                            <div class="col d-flex justify-content-center ">
                                <a href="javascript:;" class="btn btn-primary w-50"><i class="fas fa-trash"></i></a>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection