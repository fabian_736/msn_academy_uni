<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
    Crear noticia
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Crear Noticia</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{route('contents.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row-reverse p-4">
                        <div class="row">
                            <div class="col">
                                <label for="">Titulo</label>
                                <input required type="text" name="title" id="" class="form-control" placeholder="Titulo de la noticia">
                            </div>
                            <div class="col">
                                <label for="">Responsable</label>
                                <select required name="author" id="" class="form-control" required>
                                    <option value="" selected disabled>Responsable...</option>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col">
                                    <label for="">Descripción</label>
                                    <textarea name="body" id="body" cols="10" rows="5" class="form-control" placeholder="Descripcion del modulo" required></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                        </div>
                        <div class="col">
                            <div class="row">
                                <div class="col">
                                    <label for="">Adjuntar imagen</label>
                                    <input accept="image/*" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar imagen" required type="file" name="noti_banner" id="noti_banner" class="form-control" required>
                                </div>
                                <div class="col">
                                    <label for="">Fecha de caducidad</label>
                                    <input type="date" min="{{now()->format('Y-m-d')}}" name="end_date" id="end_date" class="form-control">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>