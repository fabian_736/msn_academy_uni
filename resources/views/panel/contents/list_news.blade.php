@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col my-5">
        <div class="row">
            <div class="col">
                <label for="" class="h3">Noticias</label>
            </div>
            <div class="col d-flex justify-content-end">
                @include('panel.contents.news.create')
            </div>
            @if(session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
            @endif
            @if(session('message'))
            <div class="alert alert-info">{{session('message')}}</div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
    <div class="col table-responsive">
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">Titulo</th>
                    <th scope="col">Detalles</th>
                    <th scope="col">Responsable</th>
                    <th scope="col">Fecha caducidad</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($news as $new)
                <tr>
                    <td>{{$new->title}}</td>
                    <td>{{Str::limit($new->body,20)}}</td>
                    <td>{{$new->author->name}}</td>
                    <td>{{$new->end_date}}</td>
                    <td>
                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="Editar noticia" href="{{route('contents.edit',[$new])}}" class="btn btn-primary d-inline-block"><i class="fas fa-edit"></i></a>
                        <form class="d-inline-block" action="{{route('contents.destroy',[$new])}}" method="post">
                            @csrf
                            <button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar noticia" onclick="return confirm('¿Desea eliminar esta noticia?')" type="submit" class="btn btn-danger d-inline-block"><i class="fas fa-trash"></i></a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection