<div class="row-reverse">
    <div class="col mb-3">
        <div class="row">
            <div class="col">
                <label for="title">Titulo</label>
                <input required value="{{ isset($recipe->title) ? $recipe->title : '' }}" type="text" name="title" id="title" class="form-control" placeholder="Titulo de la receta">
            </div>
            <div class="col">
                <label for="Noticia">Noticia</label>
                <select required name="new_id" id="new_id" class="form-control">
                    <option value="{{ isset($recipe->new->id)? $recipe->new->id:'' }}" selected>{{ isset($recipe->new->title)? $recipe->new->title:'Selecciona una opción...' }}</option>
                    @foreach ($news as $new)
                    <option value="{{ $new->id }}">{{ $new->title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col mb-3">
        <div class="col">
            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
        </div>
    </div>
    <div class="col mb-3">
        <div class="row">
            <div class="col">
                <label for="">Adjuntar documento</label>
                <input accept=".pdf" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar documento" type="file" name="file" id="file" class="form-control">
            </div>
            <div class="col">
                <label for="">Adjuntar imagen del banner</label>
                <input accept="image/*" data-bs-toggle="tooltip" data-bs-placement="top" title="Cargar imagen" type="file" name="image" id="image" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-12">
        <label for="description">Descripcion</label>
        <textarea placeholder="Digite una descripción" class="form-control" name="description" id="description" cols="30" rows="3">{{ isset($recipe->description)? $recipe->description:'' }}</textarea>
    </div>

</div>