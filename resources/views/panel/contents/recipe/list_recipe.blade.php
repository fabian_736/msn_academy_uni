@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col my-5">
        <div class="row">
            <div class="col">
                <label for="" class="h3">Recetas</label>
            </div>
            <div class="col d-flex justify-content-end">
                @include('panel.contents.recipe.create_recipe')
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
    <div class="col table-responsive">
        <table id="example" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">Titulo</th>
                    <th scope="col">Noticia Enlazada</th>
                    <th scope="col">Responsable</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($recipes as $recipe)
                <tr>
                    <td>{{$recipe->title}}</td>
                    <td>{{$recipe->new->title}}</td>
                    <td>{{$recipe->user->name}}</td>
                    <td>
                        <form class="d-inline-block" action="{{  route('contents.recipes.destroy',[$recipe])  }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar receta" onclick="return confirm('¿Desea Eliminar Está Receta?')" class="btn btn-danger" type="submit">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                        @include('panel.contents.recipe.edit')
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

