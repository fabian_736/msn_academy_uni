@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">

    <div class="col card-body">
        <div class="col my-5">
            <div class="row">
                <div class="col">
                    <label for="" class="h3">Editar Noticia</label>
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

        <div class="row d-flex justify-content-center g-4">
            <div class="col-8">
                <div class="card">
                    <img data-bs-toggle="tooltip" data-bs-placement="top" title="Imagen de la noticia" src="{{$new->getFirstMediaUrl('banner')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title mb-2">{{$new->title}}</h5>
                        <form action="{{route('contents.update', [$new])}}" method="post" enctype="multipart/form-data" required>
                            @csrf
                            <div class="row-reverse">
                                <div class="row">
                                    <div class="col">
                                        <label for="noticia">Titulo</label>
                                        <input type="text" name="title" id="" class="form-control" placeholder="noticia" value="{{$new->title}}" required>
                                    </div>
                                    <div class="col">
                                        <label for="">Responsable</label>
                                        <select name="author" id="" class="form-control" required>
                                            <option value="" disabled selected>Responsable...</option>
                                            @foreach($users as $user)
                                            <option value="{{$user->id}}" selected>{{$user->id}}-{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="">Adjuntar imagen</label>
                                        <input accept="image/*" type="file" name="noti_banner" id="noti_banner" class="form-control">
                                    </div>

                                    <div class="col">
                                        <label for="">Fecha de caducidad</label>
                                        <input value="{{$new->end_date}}" type="date" min="{{now()->format('Y-m-d')}}" name="end_date" id="end_date" class="form-control">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col">
                                            <label for="">Descripción</label>
                                            <textarea name="body" id="" cols="10" rows="5" class="form-control" placeholder="Descripcion del modulo" required>{{$new->body}}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer d-flex justify-content-center mt-5">
                                    <button data-bs-toggle="tooltip" data-bs-placement="top" title="Actualizar Noticia" type="submit" class="btn w-30 bg-gradient-primary">ACTUALIZAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection