<!-- Modal -->
<div class="modal fade" id="eliminarusuario" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center" style="font-size: 4em;">
                <i class="fa fa-edit "></i>
                <h3>Are you sure?</h3>
                <P>You won't be able to revert this!</P>
            </div>
            <form action="{{url('user/admin/'.$user->id)}}" method="post">
                @csrf
                @method('DELETE')
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Si, Borrar</button>
            </div>
            </form>
        </div>
    </div>
</div>