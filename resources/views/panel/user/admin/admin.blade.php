@extends('panel.layouts.app')
@section('content')

@if(session('editar'))
<div class="alert alert-success" role="alert">
    Usuario editado Con Exito
</div>
@endif

@if(session('crear'))
<div class="alert alert-success" role="alert">
    Usuario Creado Con Exito
</div>
@endif
<div class="row-reverse">
    <div class="col my-5">
        <div class="row">
            <div class="col">
                <label for="" class="h3">Administradores</label>
            </div>
            <div class="col d-flex justify-content-end">
                @include('panel.user.admin.admin_create')
            </div>
        </div>
    </div>
    <div class="col table-responsive">
        @include('panel.user.admin.admin_table')
    </div>
</div>

@endsection