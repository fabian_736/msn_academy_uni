<div class="row-reverse">
    <div class="col">
        <label for="name">Nombre</label>
        <input required  value="{{isset($user->name)? $user->name : ''}}" type="text" name="name" id="name" class="form-control @error('name') is-invalid   @enderror" placeholder="Nombre">
        <div class="invalid-feedback">
            @error('name')
            {{$message}}
            @enderror
        </div>
    </div>
    <div class="col">
        <label for="name">Apellido</label>
        <input required value="{{isset($user->surname)? $user->surname : ''}}" type="text" name="surname" id="surname" class="form-control @error('surname') is-invalid  @enderror" placeholder="Apellido">
        <div class="invalid-feedback">
            @error('surname')
            {{$message}}
            @enderror
        </div>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
        <label for="name">Correo</label>
                <input required value="{{isset($user->email)? $user->email : ''}}" type="text" name="email" id="email" class="form-control @error('email') is-invalid   @enderror" placeholder="Correo electronico">
                <div class="invalid-feedback">
                    @error('email')
                    {{$message}}
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
        <label for="name">Estado</label>
                <select required name="is_active" id="is_active" class="form-control @error('is_active') is-invalid   @enderror">
                    <option value="{{isset($user->is_active)? $user->is_active : ''}}"  selected>{{isset($user->is_active)? $user->is_active : 'Selecciona una opción...'}}</option>
                    <option value="1">Activo</option>
                    <option value="2">Inactivo</option>
                </select>
                <div class="invalid-feedback">
                    @error('is_active')
                    {{$message}}
                    @enderror
                </div>
            </div>
            <!-- Seleccione el rol... -->
            <div class="col">
                <label for="">Tipo de usuario</label>
                <select required name="user_type" id="user_type" class="form-control @error('user_type') is-invalid   @enderror">
                    <option value="{{isset($user->user_type)? $user->user_type : ''}}" selected>{{isset($user->user_type)? $user->user_type : 'Selecciona una opción...'}}</option>
                    <option value="superadmin">superadmin</option>
                    <option value="admin">admin</option>
                    <option value="patient">patient</option>
                </select>
                <div class="invalid-feedback">
                    @error('user_type')
                    {{$message}}
                    @enderror
                </div>
            </div>
        </div>
    </div>
</div>