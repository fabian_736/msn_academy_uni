<button class="btn btn-primary w-50" data-bs-toggle="modal" data-bs-target="#exampleModal2{{$user->id}}"><i class="fas fa-edit"></i></button>

<!-- Modal -->
<div class="modal fade" id="exampleModal2{{$user->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Editar Usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ url('users/admins/'.$user->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                    @include('panel.user.admin.form')
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Guardar cambios</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>