@if ($errors->any())
<div class="alert alert-warning" role="alert">
    <ul>
        @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
@endif
<table id="example" class="table table-sm table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th scope="col">Nombre completo</th>
            <th scope="col">Correo electronico</th>
            <th scope="col">Estado</th>
            <th scope="col">Tipo de usuario</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{$user->is_active}}</td>
            <td>{{ $user->user_type }}</td>
            <td>
                @include('panel.user.admin.admin_edit')
            </td>
        </tr>
        @endforeach
    </tbody>
</table>