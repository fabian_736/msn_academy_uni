@extends('panel.layouts.app')
@section('content')
<div class="row-reverse">
    <div class="col my-5">
        <div class="row">
            <div class="col">
                <label for="" class="h3">Clientes</label>
            </div>
            <div class="col d-flex justify-content-end">
                @include('panel.user.patient.client_create')

            </div>
        </div>
    </div>
    @include('panel.user.patient.client_table')
</div>
@endsection