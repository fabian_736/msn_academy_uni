    <div class="row-reverse">
    <div class="col mb-3">
        <div class="row">
            <div class="col">
                <input required value="{{isset($patient->user->name)? $patient->user->name : ''}}" type="text" name="name" id="name" class="form-control" placeholder="Nombre" >
            </div>
            <div class="col">
                <input required value="{{isset($patient->user->surname)? $patient->user->surname : ''}}" type="surname" name="surname" id="surname" class="form-control" placeholder="Apellido">
            </div>
        </div>
    </div>
    <div class="col mb-3">
        <div class="row">
            <div class="col">
                <input required value="{{isset($patient->user->email)? $patient->user->email : ''}}" type="email" name="email" id="" class="form-control" placeholder="Correo electronico">
            </div>
        </div>
    </div>
    <div class="col mb-3">
        <div class="row">
            <div class="col">
                <input required value="{{isset($patient->address)? $patient->address : ''}}" type="text" name="address" id="" class="form-control" placeholder="Direccion" >
            </div>
            <div class="col">
                <input required value="{{isset($patient->phone)? $patient->phone: ''}}" type="text" name="phone" id="" class="form-control" placeholder="Telefono" >
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <select required class="form-control" name="document_type_id" id="document_type_id">
                    <option value="{{isset($patient->typedocument->id)? $patient->typedocument->id: ''}}">{{isset($patient->typedocument->name)? $patient->typedocument->name: 'Selecciona una opcion...'}}</option>
                    @foreach($type_documents as $document)
                    <option value="{{$document->id}}">{{$document->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <input required value="{{isset($patient->document_number)? $patient->document_number: ''}}" type="text" name="document_number" id="document_number" class="form-control" placeholder="Numero de documento">
            </div>
        </div>
        <br>
        <div class="col-sm-12">
            <label for="name">Estado</label>
                <select required name="is_active" id="is_active" class="form-control @error('is_active') is-invalid   @enderror">
                    <option value="{{isset($patient->user->is_active)? $patient->user->is_active : ''}}"  selected>{{isset($patient->user->is_active)? $patient->user->is_active : 'Selecciona una opción...'}}</option>
                    <option value="1">Activo</option>
                    <option value="2">Inactivo</option>
                </select>
                <div class="invalid-feedback">
                    @error('is_active')
                    {{$message}}
                    @enderror
                </div>
        </div>
    </div>
    <input required value="patient" type="hidden" name="user_type" id="user_type" class="form-control">
</div> 