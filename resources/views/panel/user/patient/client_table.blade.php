@if ($errors->any())
<div class="alert alert-warning" role="alert">
    <ul>
        @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
@endif
<div class="col table-responsive">
        <table id="example" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th scope="col">Registro</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Telefono</th>
                    <th scope="col">Direccion</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($patients as $patient)
                <tr>
                    <td>{{$patient->pap}}</td>
                    <td>{{$patient->document_number}}</td>
                    <td>{{$patient->user->name}}</td>
                    <td>{{$patient->user->surname}}</td>
                    <td>{{$patient->user->email}}</td>
                    <td>{{$patient->phone}}</td>
                    <td>{{$patient->address}}</td>
                    <td>
                        @include('panel.user.patient.client_edit')
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
