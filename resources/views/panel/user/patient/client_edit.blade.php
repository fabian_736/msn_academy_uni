<button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editarcliente{{$patient->id}}">
    <i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="editarcliente{{$patient->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white" id="exampleModalLabel">Editar Usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{url('users/patients/'.$patient->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    @include('panel.user.patient.client_form')
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Editar Usuario</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>