<script>
    $(document).ready(function() {
        $('.modal').show();
    })
</script>
<div class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body p-5 mt-4 text-center">
                <img class="w-75" src="{{asset('panel/img/logo/1.png')}}" alt=""><br>
                <h3 class="mt-3 font-weight-bold mb-4"><b>¡ATENCIÓN! <br>Por favor consulta mas tarde</b></h3>
                <hr>
                <h3 class="mt-4 font-weight-bold mb-2"><b><i class="fa fa-exclamation-triangle mr-2"></i>No hay {{ $message }} disponibles</b></h3>
                <a href="{{url('home')}}" class="btn btn-primary mt-4">Volver</a>
            </div>
        </div>
    </div>
</div>
