@extends('landing/layouts.auth.app')
@section('content')
<div class="container p-0 m-0">

    <!-- Outer Row -->
    <div class="row ">

        <div class="col-xl-10 col-lg-12 col-md-12 ">

            <div class="o-hidden border-0 my-5" id="cardlogin">
                <!-- Nested Row within Card Body -->
                <div class="d-flex flex-row-reverse">
                    <div class="col-lg-6 shadow-lg mx-auto bg-white">
                        <form class="p-5" style="margin-top: 30px; margin-bottom: 30px;" action="{{ route('password.email') }}" method="POST">
                            @csrf
                            <div class="text-center ">
                                <img src="{{ url('landing/img/logo/5.png') }}" class="w-100" alt="" style="margin-bottom: 50px;">
                            </div>
                            <div class="row mx-auto text-center my-3">
                                <div class="col">
                                    <label for="" style="text-decoration: underline #312783; color: #312783; font-weight: bold">Ingrese
                                        su correo electrónico para enviarle un enlace de recuperación</label>
                                </div>
                            </div>

                            @if (session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                            @endif
                            @if (session('status'))
                            <div class="alert alert-success">{{ session('status') }}</div>
                            @endif
                            @if (session('message'))
                            <div class="alert alert-info">{{ session('message') }}</div>
                            @endif
                            @if (session('error'))
                            <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif
                            <div class="form-group">
                                <input type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Correo electronico" name="email">
                            </div>
                            @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <button type="submit" class="btn btn-user btn-block mt-5" style="border-radius: 40px; color: white;  background-color: #312783; font-weight: bold">Restablecer
                                contraseña</button>
                            <div class="text-center">
                                <a class="btn-icon-split text-white" href="{{ route('login') }}">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-key" style="color: #312783"></i>
                                    </span>
                                    <span style="color: #312783; font-weight: bold">Iniciar sesión</span>
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
@endsection