@extends('landing/layouts.auth.app')
@section('content')
    <div class="container p-0 m-0">

        <!-- Outer Row -->
        <div class="row ">

            <div class="col-xl-10 col-lg-12 col-md-12 ">

                <div class="o-hidden border-0 my-5" id="cardlogin">
                    <!-- Nested Row within Card Body -->
                    <div class="d-flex flex-row-reverse">
                        <div class="col-lg-6 shadow-lg mx-auto bg-white">
                            <form class="p-5" style="margin-top: 30px; margin-bottom: 30px;"
                                action="{{ route('verification.send') }}" method="POST">
                                @csrf
                                <div class="text-center ">
                                    <img src="{{ url('landing/img/logo/5.png') }}" class="w-100" alt=""
                                        style="margin-bottom: 50px;">
                                </div>
                                @if (session('status') == 'verification-link-sent')
                                    <div class="alert alert-success">Se ha enviado un nuevo enlace para verificar su correo
                                        electrónico</div>
                                @endif
                                <button type="submit" class="btn btn-user btn-block mt-5"
                                    style="border-radius: 40px; color: white;  background-color: #312783; font-weight: bold">Reenviar
                                    correo electrónico de confirmación</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
