<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Tests\TestCase;

class EmailVerificationTest extends TestCase
{
    use RefreshDatabase;

    public function test_el_usuario_es_redireccionado_a_la_página_de_verificacion_de_email()
    {
        $user = User::factory()->unverified()->create();

        $this->followingRedirects()
            ->actingAs($user)
            ->get(route('users.admins.index'))
            ->assertOk()
            ->assertViewIs('auth.verify-email');
    }

    public function test_el_usuario_puede_reenviar_el_email_de_verificacion()
    {
        $user = User::factory()->unverified()->create();

        $this->expectsNotification($user, VerifyEmail::class);

        $this->actingAs($user)
            ->from(route('verification.notice'))
            ->post(route('verification.send'))
            ->assertSessionHas('status', 'verification-link-sent');
    }

    public function test_el_usuario_verifica_su_email()
    {
        /** @var User */
        $user = User::factory()->create();

        // El método para crear la url fue tomado de Illuminate\Auth\Notifications\VerifyEmail
        // debido a que el método que la genera en esa notificación no es público
        $url = URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $user->getKey(),
                'hash' => sha1($user->getEmailForVerification()),
            ]
        );

        $this->actingAs($user)
            ->get($url)
            ->assertRedirect(route('index', ['verified' => 1]));

        $user->refresh();

        $this->assertNotNull($user->email_verified_at);
    }
}
