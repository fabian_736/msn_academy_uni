<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UpdatePasswordTest extends TestCase
{
    use RefreshDatabase;

    public function test_usuario_puede_ver_el_formulario_de_actualizacion_de_contrasena()
    {
        /** @var User */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('profile.index'))
            ->assertOk()
            ->assertViewIs('panel.perfil.index');
    }

    public function test_el_sistema_valida_que_la_contrasena_actual_es_incorrecta()
    {
        /** @var User */
        $user = User::factory()->create([
            'password' => Hash::make('Mi contraseña segura'),
        ]);
        $data = [
            'current_password' => 'contraseña inválida',
            'password' => 'mi nueva contraseña',
            'password_confirmation' => 'mi nueva contraseña',
        ];

        $this
            ->actingAs($user)
            ->put(route('user-password.update'), $data)
            ->assertInvalid(['current_password'], 'updatePassword');
    }

    public function test_usuario_no_puede_cambiar_la_contrasena_sin_confirmar_la_nueva_contrasena()
    {
        /** @var User */
        $user = User::factory()->create([
            'password' => Hash::make($password = 'Mi contraseña segura'),
        ]);
        $this
            ->actingAs($user)
            ->put(
                route('user-password.update'),
                [
                    'current_password' => $password,
                    'password' => 'mi nueva contraseña',
                    'password_confirmation' => 'mi nueva contraseña mal confirmada',
                ]
            )
            ->assertInvalid(['password'], 'updatePassword');
    }

    public function test_usuario_actualiza_su_contrasena()
    {
        /** @var User */
        $user = User::factory()->create([
            'password' => Hash::make($currentPassword = 'Mi contraseña segura'),
        ]);

        $newPassword = 'mi nueva contraseña';

        $this
            ->actingAs($user)
            ->put(
                route('user-password.update'),
                [
                    'current_password' => $currentPassword,
                    'password' => $newPassword,
                    'password_confirmation' => $newPassword,
                ]
            )
            ->assertSessionHas(
                key: 'status',
                value: 'password-updated'
            );

        $user->refresh();

        $this->assertTrue(Hash::check($newPassword, $user->password));
    }
}
