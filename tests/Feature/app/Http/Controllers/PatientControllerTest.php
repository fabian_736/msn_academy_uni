<?php

namespace Tests\Feature\app\Http\Controllers;

use App\Enums\UserTypes;
use App\Models\DocumentType;
use App\Models\Patient;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Spatie\WelcomeNotification\WelcomeNotification;
use Tests\TestCase;

class PatientControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_el_usuario_puede_ver_el_listado_de_pacientes()
    {
        /** @var User */
        $user = User::factory()->create([
            'user_type' => UserTypes::admin->value,
        ]);

        $patients = Patient::all();

        $this->actingAs($user)
            ->get(route('users.patients.index'))
            ->assertOk()
            ->assertViewIs('panel.user.client')
            ->assertViewHas('patients', $patients);
    }

    public function test_el_usuario_puede_crear_un_paciente()
    {
        /** @var User */
        $user = User::factory()->create();

        $document_type = DocumentType::factory()->create();

        Notification::fake();

        $this->followingRedirects()
            ->actingAs($user)
            ->from(route('users.patients.index'))
            ->post(
                route('users.patients.store'),
                [
                    'name' => 'Prueba',
                    'surname' => 'Prueba',
                    'email' => 'prueba@prueba.com',
                    'address' => 'Calle 2 N 3-80',
                    'phone' => '3005555555',
                    'document_type_id' => $document_type->id,
                    'document_number' => '1065666999',
                ]
            )
            ->assertOk()
            ->assertViewIs('panel.user.client');

        $this->assertDatabaseHas('users', [
            'name' => 'Prueba',
            'surname' => 'Prueba',
            'email' => 'prueba@prueba.com',
            'user_type' => UserTypes::patient->value,
        ]);
        $this->assertDatabaseHas('patients', [
            'address' => 'Calle 2 N 3-80',
            'phone' => '3005555555',
            'document_type_id' => $document_type->id,
            'document_number' => '1065666999',
        ]);

        $newPatient = User::whereEmail('prueba@prueba.com')->first();

        Notification::assertSentTo($newPatient, WelcomeNotification::class);
    }

    public function test_el_usuario_no_puede_crear_un_paciente()
    {
        /** @var User */
        $user = User::factory()->create();

        $document_type = DocumentType::factory()->create();

        $patient_data = [
            'name' => 'Prueba',
            'surname' => 'Prueba',
            'email' => 'pruebaprueba.com',
            'address' => 'Calle 2 N 3-80',
            'phone' => '3005555555',
            'document_type_id' => $document_type->id,
            'document_number' => '1065666999',
        ];

        $this->doesntExpectJobs(WelcomeNotification::class);

        $this->actingAs($user)
            ->from(route('users.patients.index'))
            ->post(route('users.patients.store'), $patient_data)
            ->assertInvalid(['email']);

        $this->assertDatabaseMissing('users', [
            'name' => 'Prueba',
            'surname' => 'Prueba',
            'email' => 'prueba@prueba.com',
            'user_type' => UserTypes::patient->value,
        ]);
    }

    public function test_el_usuario_puede_modificar_un_paciente()
    {
        /** @var User */
        $user = User::factory()->create();

        $document_type = DocumentType::factory()->create();

        $patient = Patient::factory()->create();

        $patient_data_update = [
            'name' => 'Prueba',
            'surname' => 'Prueba',
            'email' => 'prueba@prueba.com',
            'user_type' => UserTypes::patient->value,
            'is_active' => 1,
            'address' => 'Calle 2 N 3-80',
            'phone' => '3005555555',
            'document_type_id' => $document_type->id,
            'document_number' => '1065666999',
        ];

        $this->followingRedirects()
            ->actingAs($user)
            ->from(route('users.patients.index'))
            ->put(route('users.patients.update', ['patient' => $patient]), $patient_data_update)
            ->assertOk()
            ->assertViewIs('panel.user.client');

        $patient->refresh();

        $this->assertEquals($patient_data_update['name'], $patient->user->name);
        $this->assertEquals($patient_data_update['surname'], $patient->user->surname);
        $this->assertEquals($patient_data_update['email'], $patient->user->email);
        $this->assertEquals($patient_data_update['user_type'], $patient->user->user_type);
        $this->assertEquals($patient_data_update['is_active'], $patient->user->is_active);
        $this->assertEquals($patient_data_update['address'], $patient->address);
        $this->assertEquals($patient_data_update['phone'], $patient->phone);
        $this->assertEquals($patient_data_update['document_type_id'], $patient->document_type_id);
        $this->assertEquals($patient_data_update['document_number'], $patient->document_number);
    }

    public function test_el_usuario_no_puede_modificar_un_paciente()
    {
        /** @var User */
        $user = User::factory()->create([
            'user_type' => UserTypes::admin->value,
            'email' => $email = 'user@user.com',
        ]);

        $patient = Patient::factory()->create();

        $patient_data_update = [
            'email' => $email,
        ];

        $this->actingAs($user)
            ->from(route('users.patients.index'))
            ->put(route('users.patients.update', ['patient' => $patient]), $patient_data_update)
            ->assertInvalid();

        $patient->refresh();

        $this->assertNotEquals($email, $patient->email);
    }
}
