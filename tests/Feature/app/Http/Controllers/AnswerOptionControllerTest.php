<?php

namespace Tests\Feature\app\Http\Controllers;

use App\Models\AnswerOption;
use App\Models\Question;
use App\Models\Trainings\Course;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class AnswerOptionControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_el_usuario_puede_consultar_todas_las_preguntas_de_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        $options = AnswerOption::factory()->count(3)->create();
        $question = $options->first()->question;
        $course = $question->course;

        $this->actingAs($user)
            ->getJson(
                uri: route(
                    name: 'courses.questions.options.index',
                    parameters: ['course' => $course, 'question' => $question]
                ),
                headers: self::$HEADERS
            )
            ->assertJsonStructure(
                structure: ['message', 'status', 'data'],
                responseData: [
                    'message' => 'Opciones de respuesta obtenidas correctamente.',
                    'status' => Response::HTTP_OK,
                    'data' => $options,
                ]
            );
    }

    public function test_el_usuario_puede_crear_una_opcion_de_respuesta()
    {
        /** @var User */
        $user = User::factory()->create();

        $question = Question::factory()->create();
        $course = $question->course;

        $optionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'is_correct' => false,
        ];

        $this->actingAs($user)
            ->postJson(
                uri: route(
                    name: 'courses.questions.options.store',
                    parameters: ['question' => $question, 'course' => $course]
                ),
                data: $optionData,
                headers: self::$HEADERS
            )
            ->assertCreated()
            ->assertJsonStructure(
                structure: ['message', 'status', 'data'],
                responseData: [
                    'message' => 'Opción de respuesta creada correctamente.',
                    'status' => Response::HTTP_OK,
                    'data' => '*',
                ]
            );

        $this->assertDatabaseHas('answer_options', $optionData);
    }

    public function test_el_usuario_no_puede_crear_una_opcion_de_respuesta()
    {
        /** @var User */
        $user = User::factory()->create();

        $question = Question::factory()->create();
        $course = $question->course;

        $optionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'is_correct' => 2,
        ];

        $this->actingAs($user)
            ->postJson(
                uri: route(
                    name: 'courses.questions.options.store',
                    parameters: ['question' => $question, 'course' => $course]
                ),
                data: $optionData,
                headers: self::$HEADERS
            )
            ->assertUnprocessable()
            ->assertInvalid(['is_correct']);

        $this->assertDatabaseMissing('answer_options', $optionData);
    }

    public function test_el_usuario_puede_editar_una_opcion_de_respuesta()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var AnswerOption */
        $option = AnswerOption::factory()->create();

        /** @var Question */
        $question = $option->question;

        /** @var Course */
        $course = $question->course;

        $this->actingAs($user)
            ->getJson(
                uri: route(
                    name: 'courses.questions.options.show',
                    parameters: ['course' => $course, 'question' => $question, 'answerOption' => $option]
                ),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(
                structure: [
                    'message',
                    'data',
                    'code',
                ],
                responseData: [
                    'message' => '',
                    'data' => $option,
                    'code' => Response::HTTP_OK,
                ]
            );
    }

    public function test_el_usuario_puede_actualizar_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var AnswerOption */
        $option = AnswerOption::factory()->create();

        /** @var Question */
        $question = $option->question;

        /** @var Course */
        $course = $question->course;

        $optionData = [
            'label' => '¿Pregunta de prueba?',
            'is_correct' => true,
        ];

        $this->actingAs($user)
            ->putJson(
                uri: route(
                    name: 'courses.questions.options.update',
                    parameters: ['course' => $course, 'question' => $question, 'answerOption' => $option]
                ),
                data: $optionData,
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJson([
                'message' => 'Opción de respuesta actualizada correctamente.',
                'status' => Response::HTTP_OK,
                'data' => [],
            ]);

        $option->refresh();

        $this->assertEquals($optionData['label'], $option->label);
        $this->assertEquals($optionData['is_correct'], $option->is_correct);
    }

    public function test_el_usuario_puede_eliminar_una_opcion_de_respuesta()
    {
        /** @var User */
        $user = User::factory()->create();
        $option = AnswerOption::factory()->create();
        $question = $option->question;
        $course = $question->course;

        $this->actingAs($user)
            ->deleteJson(
                uri: route(
                    name: 'courses.questions.options.destroy',
                    parameters: ['course' => $course, 'question' => $question, 'answerOption' => $option]
                ),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJson([
                'message' => 'Opción de respuesta eliminada correctamente.',
                'status' => Response::HTTP_OK,
                'data' => [],
            ]);

        $this->assertModelMissing($option);
    }
}
