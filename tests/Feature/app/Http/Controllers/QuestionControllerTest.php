<?php

namespace Tests\Feature\app\Http\Controllers;

use App\Enums\CourseQuestionTypes;
use App\Models\Question;
use App\Models\Trainings\Course;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class QuestionControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_el_usuario_puede_consultar_todas_las_preguntas_de_un_curso()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Course */
        $course = Course::factory()->create();

        $questions = Question::factory()->for($course)->count(3)->create();

        $this->actingAs($user)
            ->getJson(route('courses.questions.index', $course), self::$HEADERS)
            ->assertJsonStructure(
                structure: ['message', 'status', 'data'],
                responseData: [
                    'message' => 'Preguntas obtenidas correctamente.',
                    'status' => Response::HTTP_OK,
                    'data' => $questions,
                ]
            );
    }

    public function test_el_usuario_puede_crear_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Course */
        $course = Course::factory()->create();

        $questionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'type' => CourseQuestionTypes::unique->value,
        ];

        $this->actingAs($user)
            ->postJson(route('courses.questions.store', $course), $questionData, self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(
                structure: ['message', 'status', 'data'],
                responseData: [
                    'message' => 'Preguntas obtenidas correctamente.',
                    'status' => Response::HTTP_OK,
                    'data' => '*',
                ]
            );

        $this->assertDatabaseHas('questions', $questionData);
    }

    public function test_el_usuario_no_puede_crear_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Course */
        $course = Course::factory()->create();

        $questionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'points' => 50,
        ];

        $this->actingAs($user)
            ->postJson(route('courses.questions.store', $course), $questionData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['type']);

        $this->assertDatabaseMissing('questions', $questionData);
    }

    public function test_el_usuario_puede_editar_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        $question = Question::factory()->create();

        /** @var Course */
        $course = $question->course;

        $this->actingAs($user)
            ->getJson(route('courses.questions.show', ['course' => $course, 'question' => $question]), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure([
                'message',
                'data',
                'code',
            ], [
                'message' => '',
                'data' => $question,
                'code' => Response::HTTP_OK,
            ]);
    }

    public function test_el_usuario_puede_actualizar_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();
        $question = Question::factory()->create();
        $course = $question->course;

        $questionData = [
            'label' => '¿Pregunta de prueba?',
            'points' => 500,
            'type' => CourseQuestionTypes::multiple->value,
        ];

        $this->actingAs($user)
            ->putJson(
                uri: route('courses.questions.update', ['course' => $course->id, 'question' => $question->id]),
                data: $questionData,
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJson([
                'message' => 'Pregunta actualizada correctamente.',
                'status' => Response::HTTP_OK,
                'data' => [],
            ]);

        $question->refresh();

        $this->assertEquals($questionData['label'], $question->label);
        $this->assertEquals($questionData['type'], $question->type);
    }

    public function test_el_usuario_puede_eliminar_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();
        $question = Question::factory()->create();
        $course = $question->course;

        $this->actingAs($user)
            ->deleteJson(
                uri: route(
                    name: 'courses.questions.destroy',
                    parameters: ['course' => $course, 'question' => $question]
                ),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJson([
                'message' => 'Pregunta eliminada correctamente.',
                'status' => Response::HTTP_OK,
                'data' => [],
            ]);

        $this->assertModelMissing($question);
    }
}
