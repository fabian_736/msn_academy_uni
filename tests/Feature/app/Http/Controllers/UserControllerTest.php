<?php

namespace Tests\Feature\app\Http\Controllers;

use App\Enums\UserTypes;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Spatie\WelcomeNotification\WelcomeNotification;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_el_usuario_puede_ver_el_listado_de_usuarios()
    {
        /** @var User */
        $user = User::factory()->create();

        User::factory()->create();

        $users = User::all();

        $this->actingAs($user)
            ->get(route('users.admins.index'))
            ->assertOk()
            ->assertViewIs('panel.user.admin.admin')
            ->assertViewHas('users', $users);
    }

    public function test_el_usuario_puede_crear_un_usuario()
    {
        /** @var User */
        $user = User::factory()->create();

        $user_data = [
            'name' => 'Prueba',
            'surname' => 'Prueba',
            'email' => 'prueba@prueba.com',
            'user_type' => UserTypes::admin->value,
            'is_active' => 1,
        ];

        Notification::fake();

        $this->followingRedirects()
            ->actingAs($user)
            ->from(route('users.admins.index'))
            ->post(route('users.admins.store'), $user_data)
            ->assertOk()
            ->assertViewIs('panel.user.admin.admin');

        $this->assertDatabaseHas('users', $user_data);

        $newUser = User::whereEmail('prueba@prueba.com')->first();

        Notification::assertSentTo($newUser, WelcomeNotification::class);
    }

    public function test_el_usuario_no_puede_crear_un_usuario_invalido()
    {
        /** @var User */
        $user = User::factory()->create();

        $user_data = [
            'name' => 'prueba',
            'surname' => 'prueba',
            'email' => 'pruebaprueba.com',
            'user_type' => UserTypes::admin->value,
            'is_active' => 1,
        ];

        $this->doesntExpectJobs(WelcomeNotification::class);

        $this->actingAs($user)
            ->from(route('users.admins.index'))
            ->post(route('users.admins.store'), $user_data)
            ->assertInvalid(['email']);

        $this->assertDatabaseMissing('users', $user_data);
    }

    public function test_el_usuario_puede_modificar_un_usuario()
    {
        /** @var User */
        $user = User::factory()->create();

        $updateUser = User::factory()->create();

        $user_data_update = [
            'name' => 'Prueba',
            'surname' => 'Prueba',
            'email' => 'prueba@prueba.com',
            'user_type' => UserTypes::admin->value,
            'is_active' => 1,
        ];

        $this->followingRedirects()
            ->actingAs($user)
            ->from(route('users.admins.index'))
            ->put(route('users.admins.update', ['admin' => $updateUser]), $user_data_update)
            ->assertOk()
            ->assertViewIs('panel.user.admin.admin');

        $updateUser->refresh();

        $this->assertEquals($user_data_update['name'], $updateUser->name);
        $this->assertEquals($user_data_update['surname'], $updateUser->surname);
        $this->assertEquals($user_data_update['email'], $updateUser->email);
        $this->assertEquals($user_data_update['user_type'], $updateUser->user_type);
        $this->assertEquals($user_data_update['is_active'], $updateUser->is_active);
    }

    public function test_el_usuario_no_puede_modificar_un_usuario()
    {
        /** @var User */
        $user = User::factory()->create([
            'user_type' => UserTypes::admin->value,
            'email' => $email = 'user@user.com',
        ]);

        $otherUser = User::factory()->create();

        $user_data_update = [
            'email' => $email,
        ];

        $this
            ->actingAs($user)
            ->from(route('users.admins.index'))
            ->put(route('users.admins.update', ['admin' => $otherUser]), $user_data_update)
            ->assertInvalid();

        $otherUser->refresh();

        $this->assertNotEquals($email, $otherUser->email);
    }
}
