<?php

use App\Http\Controllers\AnswerOptionController;
use App\Http\Controllers\Auth\WelcomeController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PanelHomeController;
use App\Http\Controllers\Patient\CourseController as PatientCourseController;
use App\Http\Controllers\Patient\ProfileController as PatientProfileController;
use App\Http\Controllers\Patient\TrainingController as PatientTrainingController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PatientHomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\RecipesController;
use App\Http\Controllers\RutasController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Spatie\WelcomeNotification\WelcomesNewUsers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::view('/home', 'landing.patient.portal.index');
    Route::view('/cuestionario', 'landing.patient.cuestionario.index');
    Route::view('/cuestionario/2', 'landing.patient.cuestionario.index_2');

    Route::group(['middleware' => ['web', WelcomesNewUsers::class]], function () {
        Route::get('welcome/{user}', [WelcomeController::class, 'showWelcomeForm'])->name('welcome');
        Route::post('welcome/{user}', [WelcomeController::class, 'savePassword']);
    });

    Route::get('', HomeController::class)->name('index');

    Route::controller(ProfileController::class)->prefix('admin/profile')->as('admin.profile.')
        ->group(function () {
            Route::get('', 'index')->name('index');
            Route::match(['put', 'patch'], '', 'update')->name('update');
        });

    Route::middleware('can:is-administrator')->group(function () {
        Route::get('portal_panel', PanelHomeController::class)->name('portal_panel');

        Route::middleware(['verified'])->group(function () {
            Route::prefix('users')->name('users.')->group(function () {
                Route::apiResource('admins', UserController::class, ['except' => ['destroy']]);
                Route::apiResource('patients', PatientController::class, ['except' => ['destroy']])->withoutMiddleware(['auth', 'verified', 'can:is-administrator']);
            });

            Route::controller(CourseController::class)->prefix('trainings')->as('trainings.')
                ->group(function () {
                    Route::get('list_courses', 'index')->name('list_courses');
                });

            Route::controller(TrainingController::class)->prefix('trainings')->as('trainings.')
                ->group(function () {
                    Route::get('index', 'preview')->name('index');
                    Route::post('save', 'store')->name('store');
                    Route::get('list_modules', 'index')->name('list_modules');
                    Route::get('/module/edit/{id}', 'edit')->name('edit');
                    Route::post('/module/update/{id}', 'update')->name('update');
                    Route::post('/module/destroy/{id}', 'destroy')->name('destroy');
                });
            Route::controller(ContentController::class)->prefix('contents')->as('contents.')
                ->group(function () {
                    Route::get('index', 'preview')->name('index');
                    Route::post('save', 'store')->name('store');
                    Route::get('list_news', 'list')->name('list_news');
                    Route::get('details/{id}', 'show')->name('show');
                    Route::get('/new/edit/{id}', 'edit')->name('edit');
                    Route::post('/content/update/{id}', 'update')->name('update');
                    Route::post('/content/destroy/{id}', 'destroy')->name('destroy');
                });
            Route::controller(RecipesController::class)->prefix('contents')->as('contents.')
                ->group(function () {
                    route::Resource('recipes', RecipesController::class);
                });

            Route::controller(PostController::class)->prefix('blog')->as('blog.')
                ->group(function () {
                    Route::get('publications', 'list')->name('publications');
                    Route::post('publications', 'store')->name('publications.store');
                    Route::post('publications/update/{post}', 'update')->name('publications.update');
                    Route::post('publications/destroy/{post}', 'destroy')->name('publications.destroy');
                    Route::get('comments/chat', 'chat')->name('comments_chat');
                    Route::post('comments/post-admin', 'admin_post_store')->name('admin_post_store');
                });

            Route::controller(CommentController::class)->prefix('blog')->name('blog.')
                ->group(function () {
                    Route::get('comments', 'list')->name('comments');
                    Route::patch('comments/edit-post/{post}', 'user_update_status_post')->name('comments.post');
                });

            Route::controller(RutasController::class)->prefix('blog')->name('blog.')
                ->group(function () {
                    Route::get('interactions', 'blog_interactions')->name('interactions');
                });

            Route::controller(PostController::class)
                ->group(function () {
                    Route::get('publications', 'list')->name('publications');
                    Route::post('publications', 'store')->name('publications.store');
                    Route::post('publications/update/{post}', 'update')->name('publications.update');
                    Route::post('publications/destroy/{post}', 'destroy')->name('publications.destroy');
                });

            Route::controller(CommentController::class)
                ->group(function () {
                    Route::get('comments', 'list')->name('comments');
                });

            Route::controller(RutasController::class)
                ->group(function () {
                    Route::get('interactions', 'blog_interactions')->name('interactions');
                });
        });

        Route::controller(CourseController::class)->prefix('courses')->name('courses.')
            ->group(function () {
                Route::post('save', 'store')->name('store');
                Route::get('/edit/{course}', 'edit')->name('edit');
                Route::post('/update/{course}', 'update')->name('update');
                Route::post('/destroy/{course}', 'destroy')->name('destroy');
            });

        Route::prefix('questions')->name('questions.')->group(function () {
            route::Resource('questions', QuestionController::class)->names('questions');
            route::Resource('answers', AnswerOptionController::class)->names('answers');
        });
    });
    Route::middleware('can:is-patient')->group(function () {
        Route::get('tutorial/index', [PatientController::class, 'tutorial_patient'])->name('tutorial_patient.index');
        Route::get('index', PatientHomeController::class)->name('portal_patient.index');

        Route::controller(PatientProfileController::class)->prefix('perfil')->name('perfil.')
            ->group(function () {
                Route::get('', 'index')->name('index');
                Route::patch('', 'update')->name('update');
                Route::patch('/image', 'update_profile_image')->name('update_image');
            });

        Route::middleware(['verified'])->group(function () {
            Route::controller(PostController::class)
                ->group(function () {
                    Route::get('blog', 'patient_post_list')->name('content.patient_post_list');
                    Route::get('blog/post/{post}', 'patient_post_show')->name('blog_patient.show');
                    Route::post('blog/post/comment', 'patient_post_store')->name('blog_patient.store');
                    Route::get('post/like/{post}', 'patient_like')->name('blog.like');
                });

            Route::controller(PatientController::class)
                ->group(function () {
                    Route::get('cuestionario/2', 'cuestionario2_patient')->name('cuestionario2_patient.index');
                    Route::get('noticias', 'noticias_patient')->name('noticias_patient.index');
                });

            Route::controller(QuestionController::class)
                ->group(function () {
                    Route::get('cuestionario/{course}', 'index')->name('cuestionario_patient.index');
                });

            Route::controller(PatientTrainingController::class)->prefix('modules')->as('modules.')
                ->group(function () {
                    Route::get('', 'index')->name('index');
                    Route::get('view/{training}', 'verify')->name('verify');
                    Route::get('{patientTraining}', 'show')->name('show');
                });

            Route::controller(PatientCourseController::class)->prefix('modules')->as('modules.')
                ->group(function () {
                    Route::get('{patientTraining}/questionnaire/{course}', 'index')->name('questionnaire');
                    Route::post('{patientTraining}/questionnaire/{patientCourse}', 'questionnaire')->name('questionnaire.index');
                    Route::get('{patientTraining}/questionnaire/{patientCourse}/retry', 'retry')->name('questionnaire.retry');
                });

            Route::controller(ContentController::class)
                ->group(function () {
                    Route::get('contents/list_news_patient', 'list_news_patient')->name('contents.list_news_patient');
                    Route::get('new/details/{id}', 'show')->name('content.show');
                });
        });
    });
});
