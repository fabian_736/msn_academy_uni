<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_course_has_response_choices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('answer_option_id')->index();
            $table->timestamps();
            $table->foreignId('patient_course_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('answer_option_id')
                ->references('id')
                ->on('answer_options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_course_has_response_choices');
    }
};
