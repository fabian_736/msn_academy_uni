<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('points')->default(0);
            $table->string('email')->unique();
            $table->enum('user_type', ['superadmin', 'administrator', 'patient']);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->enum('is_active', ['Activo', 'Inactivo'])->default('Activo');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
