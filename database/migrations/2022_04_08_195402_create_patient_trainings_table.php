<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_trainings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('training_id')->index();
            $table->unsignedBigInteger('patient_id')->index();
            $table->integer('points')->default(0);
            $table->datetime('start_date');
            $table->datetime('end_date')->nullable();
            $table->timestamps();
            $table->foreign('training_id')
                ->references('id')
                ->on('trainings');
            $table->foreign('patient_id')
                ->references('id')
                ->on('patients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_trainings');
    }
};
