<?php

namespace Database\Seeders;

use App\Models\Pathology;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CreatePathologiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $patologias = [
            'Cancer',
            'Hipertensión',
            'Diabetes',
            'Madres gestantes',
        ];

        foreach ($patologias as $patologia_db) {
            $patologia_new = new Pathology();
            $patologia_new->name = $patologia_db;
            $patologia_new->created_at = Carbon::now();
            $patologia_new->updated_at = Carbon::now();
            $patologia_new->save();
        }
    }
}
