<?php

namespace Database\Seeders;

use App\Models\DocumentType;
use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocumentType::insert([
            'name' => 'DNI',
            'code' => '1',
        ]);

        DocumentType::insert([
            'name' => 'Cédula',
            'code' => '2',
        ]);

        DocumentType::insert([
            'code' => '3',
            'name' => 'Pasaporte',
        ]);
    }
}
