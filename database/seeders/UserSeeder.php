<?php

namespace Database\Seeders;

use App\Enums\UserTypes;
use App\Models\Patient;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'email' => 'root@test.com',
            'user_type' => UserTypes::superadmin->value,
            'password' => Hash::make('1234'),
        ]);

        User::factory()->create([
            'email' => 'admin@test.com',
            'user_type' => UserTypes::admin->value,
            'password' => Hash::make('1234'),
        ]);

        $patient = User::factory()->create([
            'email' => 'paciente@test.com',
            'user_type' => UserTypes::patient->value,
            'password' => Hash::make('1234'),
            
        ]);

        Patient::factory()->for($patient, 'user')->create();
    }
}
