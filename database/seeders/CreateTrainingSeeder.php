<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CreateTrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Trainings\Training::factory(5)->create();
    }
}
