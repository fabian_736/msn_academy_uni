<?php

namespace Database\Seeders;

use App\Models\Blog\Category;
use Illuminate\Database\Seeder;

class CreateCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $categories = [
        //     ['name' => 'post', 'description' => 'Permite comentarios'],
        //     ['name' => 'page', 'description' => 'No permite comentarios'],

        // ];
        // foreach ($categories as $category) {
        //     Category::create($category);
        // }

        Category::insert([
            'name' => 'global', 'description' => 'Permite comentarios', 'active' => 1, 'created_at' => now(), 'updated_at' => now(),
        ]);
    }
}
