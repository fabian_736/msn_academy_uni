<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            CreatePathologiesSeeder::class,
            DocumentTypeSeeder::class,
            CreateCategorySeeder::class,
        ]);
        \App\Models\Trainings\Training::factory(2)->create();
        // \App\Models\DocumentType::factory(5)->create();
        \App\Models\Trainings\Course::factory(2)->create();
        // \App\Models\Content\News::factory(2)->create();
        // \App\Models\Recipes::factory(2)->create();
        // \App\Models\Blog\Post::factory(2)->create();
        // \App\Models\Blog\Comment::factory(2)->create();
        // \App\Models\AnswerOption::factory(2)->create();
        // \App\Models\Question::factory(2)->create();
    }
}
