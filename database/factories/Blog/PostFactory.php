<?php

namespace Database\Factories\Blog;

use App\Models\Blog\Category;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Blog\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $start_date = Carbon::instance($this->faker->dateTimeBetween('-1 months', '+1 months'));
        $end_date = (clone $start_date)->addDays(random_int(0, 100));

        return [
            'title' => $this->faker->text(50),
            'body' => $this->faker->text(250),
            'publication_starts_at' => $start_date,
            'publication_ends_at' => $end_date,
            'order' => $this->faker->randomDigitNotZero(),
            'author_id' => User::inRandomOrder()->where('user_type', 'administrator')->first()->id,
            'category_id' => Category::inRandomOrder()->first()->id,
            'active' => $this->faker->boolean(),
        ];
    }
}
