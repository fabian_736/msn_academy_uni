<?php

namespace Database\Factories;

use App\Models\DocumentType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Patient>
 */
class PatientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'address' => $this->faker->address(),
            'phone' => $this->faker->tollFreePhoneNumber(),
            'document_type_id' => DocumentType::factory(),
            'document_number' => $this->faker->randomNumber($nbDigits = null, $strict = false),
        ];
    }
}
