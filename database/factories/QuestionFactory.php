<?php

namespace Database\Factories;

use App\Enums\CourseQuestionTypes;
use App\Models\Trainings\Course;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Question>
 */
class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'course_id' => Course::factory(),
            // 'type' => $this->faker->randomElement(['unique', 'multiple']),
            'type' => $this->faker->randomElement(['unique']),
            'label' => $this->faker->text(100),
        ];
    }

    /**
     * Indicate that the model's type should be "multiple".
     *
     * @return static
     */
    public function typeMultiple()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => CourseQuestionTypes::multiple->value,
            ];
        });
    }
}
