<?php

namespace Database\Factories\Trainings;

use App\Models\Trainings\Training;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Trainings\Course>
 */
class CourseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(20),
            'training_id' => Training::inRandomOrder()->first()->id,
            'description' => $this->faker->text(),
            'Url' => $this->faker->url(),
        ];
    }
}
