<?php

namespace Database\Factories\Content;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Content\News>
 */
class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(30),
            'body' => $this->faker->text(1500),
            'end_date' => $this->faker->date,
            'author_id' => User::inRandomOrder()->where('user_type', 'administrator')->first()->id,
        ];
    }
}
