$(document).on('change', 'input[type="file"]', function() {
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 70 * 1024 * 1024) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'El archivo no debe superar los 3MB',
            showConfirmButton: false,
            timer: 1500
        })
        this.value = '';
        this.files[0].name = '';
    } else {
        // recuperamos la extensión del archivo
        var ext = fileName.split('.').pop();
        ext = ext.toLowerCase();

        switch (ext) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'pdf':
            case 'ogg':
            case 'mp3':
                break;
            default:
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'El archivo no tiene la extensión adecuada',
                    showConfirmButton: false,
                    timer: 1500
                })
                this.value = ''; // reset del valor
                this.files[0].name = '';
        }
    }
    Swal.fire({
        icon: 'success',
        title: 'Archivo cargado con exito',
        showConfirmButton: true,
        timer: 1500
    })
});