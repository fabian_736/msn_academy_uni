const edit = () => {
    $(".edit").hide('slow');
    $(".confirm").show('slow');

    $("#input-1").show('slow');
    $("#print-1").hide('slow');

    $("#input-2").show('slow');
    $("#print-2").hide('slow');

    $("#input-3").show('slow');
    $("#print-3").hide('slow');

    $("#input-4").show('slow');
    $("#print-4").hide('slow');

    $("#input-5").show('slow');
    $("#print-5").hide('slow');

    $("#input-8").show('slow');
    $("#print-8").hide('slow');

    $(".bottom_update").show('slow');
};

const confirm = () => {
    $(".edit").show('slow');
    $(".confirm").hide('slow');

    $("#input-1").hide('slow');
    $("#print-1").show('slow');

    $("#input-2").hide('slow');
    $("#print-2").show('slow');

    $("#input-3").hide('slow');
    $("#print-3").show('slow');

    $("#input-4").hide('slow');
    $("#print-4").show('slow');

    $("#input-5").hide('slow');
    $("#print-5").show('slow');

    $(".bottom_update").hide('slow');

};