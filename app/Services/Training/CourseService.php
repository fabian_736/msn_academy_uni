<?php

namespace App\Services\Training;

use App\Models\Trainings\Course;

class CourseService
{
    /**
     * @return [type]
     */
    public static function list()
    {
        $courses = Course::get();

        return $courses;
    }

    /**
     * @param  mixed  $request
     * @return [type]
     */
    public static function store($request)
    {
        $course = new Course();
        $course->name = $request->name;
        $course->description = $request->description;
        $course->training_id = $request->training_id;
        $course->URL = $request->URL;
        $course->save();
        if ($request->hasFile('document') && $request->file('document')->isValid()) {
            $course->addMediaFromRequest('document')->toMediaCollection('media');
        }
        if ($request->hasFile('ban') && $request->file('ban')->isValid()) {
            $course->addMediaFromRequest('ban')->toMediaCollection('banner');
        }

        return $course;
    }

    /**
     * @param  mixed  $request
     * @param  mixed  $course
     * @return [type]
     */
    public static function update($request, $course)
    {
        $course->name = $request->name;
        $course->status = $request->status;
        $course->description = $request->description;
        $course->training_id = $request->training_id;
        $course->URL = $request->URL;
        $course->save();
        if ($request->hasFile('document') && $request->file('document')->isValid()) {
            $course->addMediaFromRequest('document')->toMediaCollection('media');
        }
        if ($request->hasFile('ban') && $request->file('ban')->isValid()) {
            $course->addMediaFromRequest('ban')->toMediaCollection('banner');
        }

        return $course;
    }
}
