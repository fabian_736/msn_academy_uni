<?php

namespace App\Services\Training;

use App\Models\Trainings\DocumentTraining;

class DocumentService
{
    /**
     * @param  mixed  $request
     * @param  mixed  $training
     * @return [type]
     */
    public static function store($request, $training)
    {
        if ($request->hasFile('document')) {
            $filename = $request->document->getClientOriginalName();
            $request->document->storeAs('modulos/'.$training->id.'/'.$filename, 'public');
            $document_training = new DocumentTraining();
            $document_training->nombre = $filename;
            $document_training->documento = 'modulos/'.$training->id.'/'.$filename;
            $document_training->FK_id_capacitacion = $training->id_capacitacion;
            $document_training->save();
        }
    }

    /**
     * @param  mixed  $request
     * @param  mixed  $training
     * @return [type]
     */
    public static function update($request, $training)
    {
        $document_training = DocumentTraining::where('FK_id_capacitacion', $training->id_capacitacion)->first();
        if ($request->hasFile('document')) {
            $filename = $request->document->getClientOriginalName();
            $request->document->storeAs('modulos/'.$training->id.'/'.$filename, 'public');
            $document_training->nombre = $filename;
            $document_training->documento = 'modulos/'.$training->id.'/'.$filename;
            $document_training->FK_id_capacitacion = $training->id_capacitacion;
            $document_training->save();
        }
    }

    /**
     * @param  mixed  $training
     * @return [type]
     */
    public static function destroy($training)
    {
        $document_training = DocumentTraining::where('FK_id_capacitacion', $training->id_capacitacion)->first();
        if ($document_training) {
            $document_training->delete();
        }
    }
}
