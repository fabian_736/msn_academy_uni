<?php

namespace App\Services\Training;

use App\Models\PatientCourse;

class PatientCourseService
{
    /**
     * create PatientCourse.
     *
     * @param  array  $input
     * @return PatientCourse
     */
    public function store(array $input)
    {
        return PatientCourse::create($input);
    }

    public function update(PatientCourse $patientCourse, array $input)
    {
        return $patientCourse->update($input);
    }
}
