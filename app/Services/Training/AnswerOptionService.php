<?php

namespace App\Services\Training;

use App\Models\AnswerOption;
use App\Models\Question;

class AnswerOptionService
{
    /**
     * Return all questions options for the given question.
     *
     * @param  Question  $question
     * @return \Illuminate\Database\Eloquent\Collection<int, App\Models\AnswerOption>
     */
    public function list(Question $question)
    {
        return AnswerOption::where('question_id', $question->id)->get();
    }

    /**
     * Creates a new question option.
     *
     * @param  array  $input
     * @return AnswerOption
     */
    // public function store(array $input): AnswerOption
    // {
    //     $answerOption = AnswerOption::create($input);

    //     if (!empty($input['image'])) {
    //         $answerOption->addMedia($input['image'])
    //             ->toMediaCollection('image');
    //     }

    //     return $answerOption;
    // }
    public static function store(array $input)
    {
        $answerOption = AnswerOption::create($input);
        // $answerOption->addMediaFromRequest('label_photo')->toMediaCollection('banner_answer');
        return $answerOption;
    }

    /**
     * Update the given question option.
     *
     * @param  AnswerOption  $answerOption
     * @param  array  $input
     * @return bool
     */
    public function update(AnswerOption $answerOption, array $input): bool
    {
        if (! empty($input['image'])) {
            $answerOption->addMedia($input['image'])
                ->toMediaCollection('image');
        }

        return $answerOption->update($input);
    }

    /**
     * Destroy the given question option.
     *
     * @param  AnswerOption  $answerOption
     * @return bool|null
     */
    public function destroy(AnswerOption $answerOption): ?bool
    {
        return $answerOption->delete();
    }
}
