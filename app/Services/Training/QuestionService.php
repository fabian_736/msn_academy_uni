<?php

namespace App\Services\Training;

use App\Models\Question;
use App\Models\Trainings\Course;

class QuestionService
{
    /**
     * Return all questions for the given course.
     *
     * @param  Course  $course
     * @return \Illuminate\Database\Eloquent\Collection<int, App\Models\Question>
     */
    public function list(Course $course)
    {
        return Question::where('course_id', $course->id)->get();
    }

    /**
     * Creates a new question.
     *
     * @param  array  $input
     * @return Question
     */
    public static function store(array $input)
    {
        $question = Question::create($input);

        return $question;
    }

    /**
     * Update the given question.
     *
     * @param  Question  $question
     * @param  array  $input
     * @return bool
     */
    public function update(Question $question, array $input): bool
    {
        return $question->update($input);
    }

    /**
     * Destroy the given question.
     *
     * @param  Question  $question
     * @return bool|null
     */
    public static function destroy(Question $question): ?bool
    {
        return  $question->delete();
    }
}
