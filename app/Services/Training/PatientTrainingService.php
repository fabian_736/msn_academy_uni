<?php

namespace App\Services\Training;

use App\Models\PatientTraining;

class PatientTrainingService
{
    /**
     * create patienttraining.
     *
     * @param  array  $input
     * @return PatientTraining
     */
    public function store(array $input)
    {
        return PatientTraining::create($input);
    }
}
