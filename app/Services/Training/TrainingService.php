<?php

namespace App\Services\Training;

use App\Models\Trainings\Training;

class TrainingService
{
    /**
     * @return [type]
     */
    public static function list()
    {
        $trainings = Training::get();

        return $trainings;
    }

    /**
     * @param  mixed  $request
     * @return [type]
     */
    public static function store($request)
    {
        $training = new Training();
        $training->name = $request->name;
        $training->description = $request->description;
        $training->type = $request->type;
        $training->save();
        if ($request->hasFile('document') && $request->file('document')->isValid()) {
            $training->addMediaFromRequest('document')->toMediaCollection('media');
        }
        if ($request->hasFile('cur_banner') && $request->file('cur_banner')->isValid()) {
            $training->addMediaFromRequest('cur_banner')->toMediaCollection('banner');
        }

        return $training;
    }

    /**
     * @param  mixed  $request
     * @param  mixed  $training
     * @return [type]
     */
    public static function update($request, $training)
    {
        $training->name = $request->name;
        $training->description = $request->description;
        $training->type = $request->type;
        $training->save();
        if ($request->hasFile('document') && $request->file('document')->isValid()) {
            $training->addMediaFromRequest('document')->toMediaCollection('media');
        }
        if ($request->hasFile('mod_banner') && $request->file('mod_banner')->isValid()) {
            $training->addMediaFromRequest('mod_banner')->toMediaCollection('banner');
        }

        return $training;
    }
}
