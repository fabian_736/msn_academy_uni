<?php

namespace App\Services;

use App\Models\Pathology;

class PathologyService
{
    /**
     * @return [type]
     */
    public static function list()
    {
        return  Pathology::get();
    }
}
