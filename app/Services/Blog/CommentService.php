<?php

namespace App\Services\Blog;

use App\Models\Blog\Comment;
use Illuminate\Support\Facades\Auth;

class CommentService
{
    public static function store($request)
    {
        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->active = 0;
        $comment->post_id = $request->post_id;
        $comment->user_id = Auth::user()->id;
        $comment->save();
    }
}
