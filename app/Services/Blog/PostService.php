<?php

namespace App\Services\Blog;

use App\Models\Blog\Post;

class PostService
{
    /**
     * @param  mixed  $request
     * @return [type]
     */
    public static function store(array $input)
    {
        $post = Post::create($input);
        $post->addMediaFromRequest('file')->toMediaCollection('file');

        return $post;
    }

    /**
     * @param  mixed  $request
     * @param  mixed  $new
     * @return [type]
     */
    public static function update(Post $post, array $input)
    {
        $post->update($input);
        $post->addMediaFromRequest('file')->toMediaCollection('file');
    }
}
