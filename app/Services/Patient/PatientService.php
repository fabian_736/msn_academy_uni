<?php

namespace App\Services\Patient;

use App\Models\Patient;

class PatientService
{
    public static function list()
    {
        $patients = Patient::all();

        return $patients;
    }

    public static function store(array $input)
    {
        $patient = Patient::create($input);

        return $patient;
    }

    public static function update(Patient $patient, array $input)
    {
        $patient->update($input);
    }
}
