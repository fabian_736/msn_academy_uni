<?php

namespace App\Services\User;

use App\Enums\UserTypes;
use App\Models\User;

class UserService
{
    /**
     * Retorna los usuarios.
     *
     * @return Illuminate\Database\Eloquent\Collection<User>
     */
    public static function list()
    {
        $users = User::where('user_type', '!=', UserTypes::patient->value)->get();

        return $users;
    }

    /**
     * Crea un usuario.
     *
     * @param  array  $input
     * @return User
     */
    public static function store(array $input)
    {
        $user = User::create($input);

        return $user;
    }

    /**
     * Actualiza el usuario indicado.
     *
     * @param  User  $user
     * @param  array  $input
     * @return bool
     */
    public static function update(User $user, array $input): bool
    {
        return $user->update($input);
    }

    /**
     * @return [type]
     */
    public static function listAdmin()
    {
        $users = User::where('user_type', '=', 'administrator')->where('is_active', 1)->get();

        return $users;
    }
}
