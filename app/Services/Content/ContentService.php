<?php

namespace App\Services\Content;

use App\Models\Content\News;

class ContentService
{
    /**
     * @param  mixed  $request
     * @return [type]
     */
    public static function store($request)
    {
        $new = new News();
        $new->title = $request->title;
        $new->body = $request->body;
        $new->end_date = $request->end_date;
        $new->author_id = $request->author;
        if ($request->hasFile('mod_banner') && $request->file('mod_banner')->isValid()) {
            $new->addMediaFromRequest('mod_banner')->toMediaCollection('banner');
        }
        $new->save();

        return $new;
    }

    /**
     * @param  mixed  $request
     * @param  mixed  $new
     * @return [type]
     */
    public static function update($request, $new)
    {
        $new->title = $request->title;
        $new->body = $request->body;
        $new->end_date = $request->end_date;
        $new->author_id = $request->author;
        if ($request->hasFile('noti_banner') && $request->file('noti_banner')->isValid()) {
            $new->addMediaFromRequest('noti_banner')->toMediaCollection('banner');
        }
        $new->save();

        return $new;
    }
}
