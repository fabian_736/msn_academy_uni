<?php

namespace App\Enums;

enum CourseQuestionTypes: string
{
    case unique = 'unique';
    case multiple = 'multiple';
}
