<?php

namespace App\Enums;

enum UserTypes: string
{
    case superadmin = 'superadmin';
    case admin = 'administrator';
    case patient = 'patient';
}
