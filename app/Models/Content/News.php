<?php

namespace App\Models\Content;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperNews
 */
class News extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Registro de las colecciones de media del Modelo News.
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner')
            ->useFallbackUrl('/img/no_banner.jpg')
            ->singleFile();
    }
}
