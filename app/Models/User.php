<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\WelcomeNotification\ReceivesWelcomeNotification;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable implements MustVerifyEmail, HasMedia
{
    use HasFactory, Notifiable, ReceivesWelcomeNotification;
    use InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'user_type',
        'is_active',
        'points',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Obtiene el Paciente asociado al Usuario.
     *
     * @return HasOne
     */
    public function patient(): HasOne
    {
        return $this->hasOne(Patient::class)->withDefault();
    }

    /**
     * Obtiene a las Ciudades asociadas al País.
     *
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Retorna el nombre completo.
     *
     * @return Attribute
     */
    protected function fullname(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $parameters) => "{$parameters['name']} {$parameters['surname']}"
        );
    }

    /**
     * Castea el atributo name.
     *
     * @return Attribute
     */
    protected function name(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => ucwords(strtolower($value)),
            get: fn ($value) => ucwords(strtolower($value)),
        );
    }

    /**
     * Castea el atributo surname.
     *
     * @return Attribute
     */
    protected function surname(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => ucwords(strtolower($value)),
            get: fn ($value) => ucwords(strtolower($value)),
        );
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')
            ->useFallbackUrl('/img/user.png')
            ->singleFile();
    }
}
