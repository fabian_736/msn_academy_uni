<?php

namespace App\Models\Trainings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperDocumentTraining
 */
class DocumentTraining extends Model
{
    use HasFactory;

    protected $table = 'bc_capacitacion_documento';

    protected $primaryKey = 'id_capacitacion_documento';
}
