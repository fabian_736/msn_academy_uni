<?php

namespace App\Models\Trainings;

use App\Models\PatientTraining;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperTraining
 */
class Training extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, SoftDeletes;

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function patientTrainings()
    {
        return $this->hasMany(PatientTraining::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner')
            ->useFallbackUrl('/img/no_banner.jpg')
            ->singleFile();
    }
}
