<?php

namespace App\Models\Trainings;

use App\Models\PatientCourse;
use App\Models\Question;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperCourse
 */
class Course extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'training_id',
        'name',
        'description',
        'status',
        'URL',
    ];

    /**
     * Retrieves the training associated to the model.
     *
     * @return BelongsTo
     */
    public function training(): BelongsTo
    {
        return $this->belongsTo(Training::class);
    }

    /**
     * Retrieves the cuestions associated to the model.
     *
     * @return HasMany
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function patienCourse()
    {
        return $this->hasMany(PatientCourse::class);
    }

    /**
     * Strip HTML and PHP tags from the description column.
     *
     * @param  string  $value
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner')
            ->useFallbackUrl('/img/no_banner.jpg')
            ->singleFile();

        $this->addMediaCollection('media')
            ->singleFile();
    }
}
