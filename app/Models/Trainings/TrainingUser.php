<?php

namespace App\Models\Trainings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperTrainingUser
 */
class TrainingUser extends Model
{
    use HasFactory;

    protected $table = 'bc_capacitacion_user';

    protected $primaryKey = 'id_capacitacion_user';
}
