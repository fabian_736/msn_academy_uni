<?php

namespace App\Models\Blog;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperPost
 */
class Post extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'title', 'body', 'author_id', 'publication_starts_at', 'publication_ends_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'publication_starts_at' => 'date:d-m-Y',
        'publication_ends_at' => 'date:d-m-Y',
    ];

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Obtiene a los Comentarios asociadas al Post.
     *
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class)
        ->orderByDesc('created_at');
    }

    public function likes()
    {
        return $this->hasMany(LikePost::class);
    }

    public function categoy()
    {
        return $this->belongsTo(Category::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('file')
            ->useFallbackUrl('/img/no_banner.jpg')
            ->singleFile();
    }
}
