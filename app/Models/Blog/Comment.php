<?php

namespace App\Models\Blog;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperComment
 */
class Comment extends Model
{
    protected $fillable = [
        'comment', 'active', 'post_id', 'user_id',
    ];

    use HasFactory;

    /**
     * @return [type]
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return [type]
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function responses()
    {
        return $this->hasMany(Comment::class, 'parent_comment_id');
    }

    public function parentComment()
    {
        return $this->belongsTo(Comment::class, 'parent_comment_id');
    }
}
