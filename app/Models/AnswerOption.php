<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperAnswerOption
 */
class AnswerOption extends Model implements HasMedia
{
    use HasFactory,InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'label',
        'is_correct',
        'question_id',
    ];

    /**
     * Retrieves the question associated to the model.
     *
     * @return BelongsTo
     */
    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }

    public function patientChoices(): BelongsToMany
    {
        return $this->belongsToMany(PatientCourse::class, 'patient_course_has_response_choices');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner_answer')
            ->singleFile();
    }
}
