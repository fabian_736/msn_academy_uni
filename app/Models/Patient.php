<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @mixin IdeHelperPatient
 */
class Patient extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'address',
        'phone',
        'document_type_id',
        'document_number',
        'pap',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($patient) {
            $patient->pap = 'PAP'.str_pad($patient->id, STR_PAD_LEFT);
            $patient->saveQuietly();
        });
    }

    /**
     * Obtiene el Usuario asociado al Paciente.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Obtiene las Patologías asociadas al Paciente.
     *
     * @return BelongsToMany
     */
    public function pathologies(): BelongsToMany
    {
        return $this->belongsToMany(Pathology::class, 'patient_pathology');
    }

    /**
     * Obtiene el Tipo de Documento asociado al Paciente.
     *
     * @return BelongsTo
     */
    public function document_type(): BelongsTo
    {
        return $this->belongsTo(DocumentType::class);
    }

    public function typedocument()
    {
        return $this->belongsTo(DocumentType::class, 'document_type_id');
    }
}
