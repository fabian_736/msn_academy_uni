<?php

namespace App\Models;

use App\Models\Content\News;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperRecipes
 */
class Recipes extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'title', 'new_id', 'user_id', 'description',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner')
            ->singleFile();

        $this->addMediaCollection('file')
            ->singleFile();
    }

    public function user()
    {
        return $this->belongsTo(user::class);
    }

    public function new()
    {
        return $this->belongsTo(News::class);
    }
}
