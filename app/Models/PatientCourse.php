<?php

namespace App\Models;

use App\Models\Trainings\Course;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperPatientCourse
 */
class PatientCourse extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_id', 'points', 'patient_training_id', 'status', 'start_date', 'end_date',
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function patientTraining()
    {
        return $this->belongsTo(PatientTraining::class);
    }

    public function responses()
    {
        return $this->belongsToMany(AnswerOption::class, 'patient_course_has_response_choices');
    }
}
