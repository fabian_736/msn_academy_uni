<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @mixin IdeHelperPathology
 */
class Pathology extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'pathologies';

    protected $fillable = [
        'name',
    ];

    /**
     * Obtiene los Pacientes asociados a la Patología.
     *
     * @return BelongsToMany
     */
    public function patients(): BelongsToMany
    {
        return $this->belongsToMany(Patient::class, 'patient_pathology');
    }
}
