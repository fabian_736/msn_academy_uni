<?php

namespace App\Models;

use App\Models\Trainings\Training;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperPatientTraining
 */
class PatientTraining extends Model
{
    use HasFactory;

    protected $fillable = [
        'training_id', 'points', 'patient_id', 'status', 'start_date', 'end_date',
    ];

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function training()
    {
        return $this->belongsTo(Training::class);
    }

    public function courses()
    {
        return $this->hasMany(PatientCourse::class);
    }
}
