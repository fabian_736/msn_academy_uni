<?php

namespace App\Http\Requests\Blog\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'body' => 'required',
            'publication_starts_at' => 'required|date:Y-m-d',
            'publication_ends_at' => 'required|date:Y-m-d',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Titulo',
            'body' => 'Descripción',
            'publication_starts_at' => 'Fecha de inicio',
            'publication_ends_at' => 'Fecha de finalización',
        ];
    }
}
