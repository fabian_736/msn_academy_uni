<?php

namespace App\Http\Requests\Question;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id' => 'required',
            'type' => 'required',
            'label' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'course_id' => 'Curso',
            'type' => 'Tipo de Pregunta',
            'label' => 'Pregunta',
        ];
    }
}
