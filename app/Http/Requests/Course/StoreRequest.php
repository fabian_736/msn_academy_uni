<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:100',
            'description' => 'bail|required|max:250',
            // 'document' => 'bail|required|mimes:pdf',
            'URL' => 'url|required|bail',
            'training_id' => 'bail|required|exists:trainings,id',
            'cur_banner' => 'bail|required|file|image|mimes:jpg,jpeg,png',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'description' => 'Descripción',
            'document' => 'Documento',
            'training_id' => 'Modulo',
        ];
    }
}
