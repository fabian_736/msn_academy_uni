<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['name'] = 'bail|required|max:100';
        $rules['status'] = 'required';

        $rules['description'] = 'bail|required|max:250';
        $rules['training_id'] = 'bail|required|exists:trainings,id';
        $rules['URL'] = 'url|required|bail';
        if ($this->has('document')) {
            // $rules['document'] = 'bail|required|mimes:pdf';
        }
        $rules['cur_banner'] = 'bail|file|image|mimes:jpg,jpeg,png';

        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'status' => 'Estado del curso',
            'description' => 'Descripción',
            'training_id' => 'Modulo',
            'document' => 'Documento',
        ];
    }
}
