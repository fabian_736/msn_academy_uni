<?php

namespace App\Http\Requests\User;

use App\Enums\UserTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'email' => ['required', 'email:rfc,dns', Rule::unique('users', 'email')->ignore($this->admin->id)],
            'user_type' => [
                'required',
                new Enum(UserTypes::class),
            ],
            'is_active' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'surname' => 'Apellido',
            'email' => 'Correo',
            'user_type' => 'Tipo de usuario',
            'is_active' => 'Estado',
        ];
    }
}
