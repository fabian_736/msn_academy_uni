<?php

namespace App\Http\Requests\Training;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:100',
            'description' => 'bail|required|max:250',
            'document' => 'bail|required|mimes:pdf',
            'type' => ['bail', 'required', Rule::in(['General', 'Pathology'])],
            'mod_banner' => 'bail|required|file|image|mimes:jpg,jpeg,png',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'description' => 'Descripción',
            'document' => 'Docuemento',
            'type' => 'Tipo',
        ];
    }
}
