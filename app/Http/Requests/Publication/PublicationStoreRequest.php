<?php

namespace App\Http\Requests\Publication;

use Illuminate\Foundation\Http\FormRequest;

class PublicationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'bail|required|max:100',
            'body' => 'bail|required|max:250',
            'publication_starts_at' => 'required|date',
            'publication_ends_at' => 'required|date',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Titulo',
            'body' => 'Publiación',
            'publication_starts_at' => 'Fecha de inicio',
            'publication_ends_at' => 'Fecha de finalización',
        ];
    }
}
