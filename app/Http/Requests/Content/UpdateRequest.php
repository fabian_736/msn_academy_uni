<?php

namespace App\Http\Requests\Content;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['title'] = 'bail|required|max:100';
        $rules['body'] = 'bail|required';
        $rules['author'] = 'bail|required|exists:users,id';
        if ($this->has('date') && $this->date != '') {
            $rules['date'] = 'bail|required|date:Y-m-d';
        }
        if ($this->has('noti_banner')) {
            $rules['noti_banner'] = 'bail|file|image|mimes:jpg,jpeg,png';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'title' => 'Titulo',
            'description' => 'Descripción',
            'author' => 'Autor',
            'date' => 'Fecha',
            'noti_banner' => 'imagen',
        ];
    }
}
