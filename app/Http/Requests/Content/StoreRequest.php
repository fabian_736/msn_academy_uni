<?php

namespace App\Http\Requests\Content;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['title'] = 'bail|required|max:100';
        $rules['body'] = 'bail|required';
        $rules['author'] = 'bail|required|exists:users,id';
        if ($this->has('date')) {
            $rules['date'] = 'bail|required|date:Y-m-d';
        }
        $rules['noti_banner'] = 'bail|required|file|image|mimes:jpg,jpeg,png';

        return $rules;
    }

    public function attributes()
    {
        return [
            'title' => 'Titulo',
            'body' => 'Descripción',
            'author' => 'Autor',
            'date' => 'Fecha',
            'noti_banner' => 'imagen',
        ];
    }
}
