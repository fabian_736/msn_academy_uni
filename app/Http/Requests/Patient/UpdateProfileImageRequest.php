<?php

namespace App\Http\Requests\Patient;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_image' => 'required|image|max:1024|mimes:jpg,jpeg,png',
        ];
    }

    public function attributes()
    {
        return [
            'profile_image' => 'Imagen de perfil',
        ];
    }

    public function messages()
    {
        return [
            'profile_image.dimensions' => 'Las dimensiones de la :attribute no son válidas.',
        ];
    }
}
