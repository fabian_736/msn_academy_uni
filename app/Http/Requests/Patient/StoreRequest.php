<?php

namespace App\Http\Requests\Patient;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email:rfc,dns|unique:users,email',
            'address' => 'required',
            'phone' => 'required',
            'document_type_id' => 'required',
            'document_number' => 'required|max:20|min:5',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'surname' => 'Apellido',
            'email' => 'Correo electrónico',
            'address' => 'Dirección de residencia',
            'phone' => 'Número de contacto',
            'document_number' => 'Número de documento',
        ];
    }
}
