<?php

namespace App\Http\Requests\Patient;

use App\Enums\UserTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd('DSfds');

        return [
            'name' => 'required',
            'surname' => 'required',
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($this->patient->user)],
            'user_type' => [
                'required',
                new Enum(UserTypes::class),
            ],
            'is_active' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'document_type_id' => 'required',
            'document_number' => 'required|max:20|min:5',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre',
            'surname' => 'Apellido',
            'email' => 'Correo',
            'user_type' => 'Tipo de usuario',
            'is_active' => 'Estado del paciente',
            'address' => 'Dirección',
            'email' => 'Correo',
            'phone' => 'Numero de telefono',
            'document_type_id' => 'Tipo de documento',
            'document_number' => 'Numero de docuemnto',
        ];
    }
}
