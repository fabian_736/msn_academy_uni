<?php

namespace App\Http\Requests\AnswerOption;

use Illuminate\Foundation\Http\FormRequest;

class AnswerOptionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $validations = [
            'is_correct' => 'required|boolean',
            'image' => 'nullable|file|mimes:png,jpg|max:10000',
            'question_id' => 'required',
        ];
        if ($this->answer_image == 1) {
            $validations['label'] = 'required|file|mimes:png,jpg,jpng,ogg,mp3';
        } else {
            $validations['label'] = 'required|string';
        }

        return $validations;
    }

    public function attributes()
    {
        return [
            'label' => 'Respuesta',
            'is_correct' => 'Estado de la respuesta',
            'image' => 'Imagen',
            'user_type' => 'Tipo de usuario',
            'question_id' => 'Pregunta',
        ];
    }
}
