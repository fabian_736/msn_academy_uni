<?php

namespace App\Http\Requests;

use App\Enums\CourseQuestionTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required|string|between:3,255',
            'type' => ['required', new Enum(CourseQuestionTypes::class)],
            'course_id' => 'required',
        ];
    }
}
