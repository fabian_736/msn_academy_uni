<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnswerOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'is_correct' => 'required|boolean',
            'question_id' => 'required',
        ];
        if ($this->answer_image == 1) {
            $validations['image'] = 'required|file|mimes:png,jpg,jpng,ogg,mp3';
            $validations['label'] = 'exclude';
        } else {
            $validations['image'] = 'exclude';
            $validations['label'] = 'required|string|max:100';
        }

        return $validations;
    }

    public function attributes()
    {
        return [
            'label' => 'Respuesta',
            'is_correct' => 'Estado de la Respuesta',
            'image' => 'Imagen',
            'user_type' => 'Tipo de usuario',
            'question_id' => 'Pregunta',
        ];
    }
}
