<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PatientHomeController extends Controller
{
    /**
     * Retorna la vista del portal de pacientes.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Gate::authorize('is-patient');

        return view('landing.patient.portal.index');
    }
}
