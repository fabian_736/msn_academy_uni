<?php

namespace App\Http\Controllers;

use App\Http\Requests\Content\StoreRequest;
use App\Http\Requests\Content\UpdateRequest;
use App\Models\Content\News;
use App\Models\Recipes;
use App\Models\User;
use App\Services\Content\ContentService;
use App\Services\User\UserService;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * @return [type]
     */
    public function preview()
    {
        $news = News::orderBy('created_at', 'Desc')->take(3)->get();
        $recipes = Recipes::all();
        $users = UserService::listAdmin();

        return view('panel.contents.index', compact('news', 'users', 'recipes'));
    }

    /**
     * @return [type]
     */
    public function list_news_patient()
    {
        $news = News::with('author')->orderBy('created_at', 'Desc')->paginate(5);

        return view(' landing.patient.noticias.index', compact('news'));
    }

    public function list()
    {
        $news = News::with('author')->orderBy('created_at', 'Desc')->get();
        $users = User::select('id', 'name')->get();

        return view('panel.contents.list_news', compact('news', 'users'));
    }

    /**
     * @param  StoreRequest  $request
     * @return [type]
     */
    public function store(StoreRequest $request)
    {
        $new = ContentService::store($request);
        $new->addMediaFromRequest('noti_banner')->toMediaCollection('banner');
        session()->flash('success', 'Noticia creada correctamente');

        return redirect()->route('contents.list_news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new = News::find($id);

        return view('landing.patient.noticias.details', compact('new'));
    }

    /**
     * @param  mixed  $id
     * @return [type]
     */
    public function edit($id)
    {
        $users = UserService::listAdmin();
        $new = News::findOrFail($id);

        return view('panel.contents.news_edit', compact('new', 'users'));
    }

    /**
     * @param  Request  $request
     * @param  mixed  $id
     * @return [type]
     */
    public function update(UpdateRequest $request, $id)
    {
        $new = News::findOrFail($id);
        $new = ContentService::update($request, $new);
        session()->flash('success', 'Noticia actualizada correctamente');

        return redirect()->route('contents.list_news');
    }

    /**
     * @param  mixed  $id
     * @return [type]
     */
    public function destroy($id)
    {
        $new = News::findOrFail($id);
        $new->delete();
        session()->flash('success', 'Noticia eliminada correctamente');

        return redirect()->route('contents.list_news');
    }
}
