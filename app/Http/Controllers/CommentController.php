<?php

namespace App\Http\Controllers;

use App\Models\Blog\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function list()
    {
        $comments = Comment::with('user', 'post')->orderBy('created_at', 'desc')->get();

        return view('panel.blog.comments', compact('comments'));
    }

    public function user_update_status_post(Request $request, $id)
    {
        $comment = Comment::FindOrFail($id);
        $comment->fill($request->all())->save();

        return redirect()->back();
    }
}
