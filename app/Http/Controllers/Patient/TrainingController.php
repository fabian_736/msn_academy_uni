<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Models\PatientTraining;
use App\Models\Trainings\Training;
use App\Services\Training\PatientTrainingService;

class TrainingController extends Controller
{
    /** @var PatientTrainingService */
    private $service;

    /**
     * Init the controller.
     *
     * @return void
     */
    public function __construct()
    {
        $this->service = new PatientTrainingService();
    }

    /**
     * @return [type]
     */
    public function index()
    {
        $trainings = Training::orderBy('created_at', 'desc')->paginate(2);
        // $trainings->load('courses.answer');
        $trainings->load(['courses' => function ($query) {
            $query->where('status', 'Publicado');
        }]);

        return view('landing.patient.retos.trainings', compact('trainings'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PatientTraining $patientTraining)
    {
        $patientTraining->load(['training.courses', 'courses.course'])->loadSum('courses', 'points');
        $trainingCourses = $patientTraining->training->courses;

        return view('landing.patient.retos.trainings_details', compact('patientTraining', 'trainingCourses'));
    }

    public function verify(Training $training)
    {
        $patientTraining = PatientTraining::where('patient_id', auth()->user()->patient->id)
            ->where('training_id', $training->id)
            ->first();

        if (is_null($patientTraining)) {
            $input = [
                'training_id' => $training->id,
                'patient_id' => auth()->user()->patient->id,
                'start_date' => now(),
                'points' => 0,
            ];
            $patientTraining = $this->service->store($input);
        }

        return redirect()->route('modules.show', ['patientTraining' => $patientTraining]);
    }
}
