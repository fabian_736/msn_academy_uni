<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ProfileUpdateRequest;
use App\Http\Requests\Patient\UpdateProfileImageRequest;
use App\Models\Patient;

class ProfileController extends Controller
{
    public function index()
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        $user->load('patient');

        return view('landing.patient.profile.index');
    }

    public function edit(Patient $patient)
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        $user->load('patient');

        return view('landing.patient.profile.index');
    }

    public function update(ProfileUpdateRequest $request)
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        $user->update($request->all());
        $user->patient->update($request->all());
        session()->flash('success', 'Datos actualizados correctamente');

        return view('landing.patient.profile.index');
    }

    public function update_profile_image(UpdateProfileImageRequest $request)
    {
        $patient = auth()->user();
        if ($request->hasFile('profile_image') && $request->file('profile_image')->isValid()) {
            $patient->addMediaFromRequest('profile_image')->toMediaCollection('avatar');
        }
        session()->flash('success', 'Imagen actualizada correctamente');

        return redirect()->back();
    }
}
