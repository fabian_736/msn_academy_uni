<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Models\AnswerOption;
use App\Models\PatientCourse;
use App\Models\PatientTraining;
use App\Models\Question;
use App\Models\Trainings\Course;
use App\Services\Training\PatientCourseService;
use DragonCode\Support\Facades\Helpers\Str;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->service = new PatientCourseService();
    }

    public function index(PatientTraining $patientTraining, Course $course)
    {
        $patientCourse = PatientCourse::firstOrCreate(
            [
                'patient_training_id' => $patientTraining->id,
                'course_id' => $course->id,
            ],
            ['start_date' => now()]
        );

        if (! is_null($patientCourse->end_date)) {
            abort(404);
        }

        $patientCourse->load([
            'course.questions.answers' => function ($query) {
                return $query->inRandomOrder();
            },
            'course.questions' => function ($query) {
                return $query->inRandomOrder();
            },
        ]);

        return view('landing.patient.cuestionario.index', compact(['patientTraining', 'patientCourse']));
    }

    public function questionnaire(Request $request, PatientTraining $patientTraining, PatientCourse $patientCourse)
    {
        if (! is_null($patientCourse->end_date)) {
            abort(404);
        }

        $answers = $this->storeResponses($patientCourse, $request);

        /** @var Collection */
        $courseQuestion = $patientCourse->course->questions;

        $correctQuestions = $courseQuestion->filter(function (Question $question) use ($answers) {
            /** @var Collection */
            $questionOptions = $question->answers;
            /** @var Collection */
            $correctOptions = $questionOptions->filter(fn ($value) => $value->is_correct == 1);
            /** @var Collection */
            $incorrectOptions = $questionOptions->filter(fn ($value) => $value->is_correct == 0);

            // Validando que todas las respuestas correctas de la pregunta hayan sido seleccionadas por el usuario
            $hasAllCorrectResponses = $correctOptions->every(function (AnswerOption $option) use ($answers) {
                return $answers->contains($option->id);
            });

            // Validando que ninguna de las respuestas incorrectas de la pregunta hayan sido seleccionadas por el usuario
            $doesNotHasAnyIncorrectResponses = $incorrectOptions->every(function (AnswerOption $option) use ($answers) {
                return ! $answers->contains($option->id);
            });

            // Se considera que la pregunta fue respondida correctamente sí, y
            // solo sí, el usuario seleccionó todas las respuestas correctas y
            // ninguna incorrecta
            return $doesNotHasAnyIncorrectResponses && $hasAllCorrectResponses;
        });

        $message = $correctQuestions->count();
        $puntaje = $message * 5;
        $total = $courseQuestion->count();

        $this->service->update($patientCourse, [
            'end_date' => now(),
            'points' => $puntaje,
        ]);

        $patientTraining->update([
            'points' => $puntaje + $patientTraining->points,
        ]);
        $patientTraining->patient->user->update([
            'points' => $patientTraining->patient->user->points + $puntaje,
        ]);

        return redirect()->route('modules.show', $patientTraining)->with('message', $total)->with('example', $message)->with('puntaje', $puntaje);
    }

    public function retry(PatientTraining $patientTraining, PatientCourse $patientCourse)
    {
        $patientTraining->update([
            'points' => $patientTraining->points - $patientCourse->points,
        ]);

        $patientTraining->patient->user->update([
            'points' => $patientTraining->patient->user->points - $patientCourse->points,
        ]);

        $course = $patientCourse->course_id;
        $patientCourse->delete();

        return redirect()->route('modules.questionnaire', ['patientTraining' => $patientTraining, 'course' => $course]);
    }

    public function storeResponses(PatientCourse $patientCourse, $request)
    {
        $answers = $request->collect()
            ->filter(fn ($value, $key) => Str::startsWith($key, 'answer_option_'))
            ->flatten();

        /** @var array */
        $challengeQuestionsIds = $patientCourse->course->questions->pluck('id');

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\QuestionOption> */
        $challengeQuestionOptions = AnswerOption::whereIn('question_id', $challengeQuestionsIds)->get();

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\QuestionOption> */
        $selectedOptions = AnswerOption::whereIn('id', $answers)
            ->get()
            ->intersect($challengeQuestionOptions);

        $patientCourse->responses()->sync($selectedOptions);

        return  $selectedOptions;
    }
}
