<?php

namespace App\Http\Controllers;

use App\Http\Requests\AnswerOptionRequest;
use App\Models\AnswerOption;
use App\Services\Training\AnswerOptionService;
use App\Traits\JsonResponses;

class AnswerOptionController extends Controller
{
    use JsonResponses;

    /** @var AnswerOptionService */
    private $service;

    public function store(AnswerOptionRequest $request)
    {
        $answer = AnswerOptionService::store($request->safe()->toArray());
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $answer->addMediaFromRequest('image')->toMediaCollection('banner_answer');
        }

        return $request->wantsJson() ?
            response()->json([
                'message' => 'Respuesta creada con exito',
                'data' => $answer,
            ]) :
            redirect()->back()->with('status', 'Respuesta creada con exito');
    }

    public function update(AnswerOptionRequest $request, $id)
    {
        $answer = AnswerOption::FindOrFail($id);
        $answer->fill($request->safe()->toArray())->save();
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $answer->addMediaFromRequest('image')->toMediaCollection('banner_answer');
        }

        return redirect()->back()->with('message', 'a');
    }

    public function destroy($id)
    {
        AnswerOption::destroy($id);

        return redirect()->back();
    }
}
