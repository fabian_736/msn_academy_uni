<?php

namespace App\Http\Controllers;

use App\Http\Requests\Training\StoreRequest;
use App\Http\Requests\Training\UpdateRequest;
use App\Models\Trainings\Course;
use App\Models\Trainings\Training;
use App\Services\Training\TrainingService;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function preview()
    {
        $trainings = Training::withCount('courses')->orderBy('created_at', 'desc')->take(3)->get();
        $courses = Course::orderBy('created_at', 'desc')->take(3)->get();

        return view('panel.trainings.index', compact('trainings', 'courses'));
    }

    public function index()
    {
        $trainings = Training::orderBy('created_at', 'desc')->get();

        return view('panel.trainings.list_modules', compact('trainings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $training = TrainingService::store($request);
        $training->addMediaFromRequest('mod_banner')->toMediaCollection('banner');
        session()->flash('success', 'Modulo creado correctamente');

        return redirect()->route('trainings.list_modules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $training = Training::find($id);

        return view('landing.patient.retos.trainings_details', compact('training'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $training = Training::findOrFail($id);

        return view('panel.trainings.modules_edit', compact('training'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $training = Training::findOrFail($id);
        $training = TrainingService::update($request, $training);
        session()->flash('success', 'Modulo actualizado correctamente');

        return redirect()->route('trainings.list_modules');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training = Training::findOrFail($id);
        $training->delete();
        session()->flash('success', 'Modulo eliminado correctamente');

        return redirect()->route('trainings.list_modules');
    }
}
