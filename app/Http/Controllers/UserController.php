<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\User;
use App\Services\User\UserService;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = UserService::list();

        return view('panel.user.admin.admin', compact('users'));
    }

    public function store(StoreRequest $request)
    {
        $user = UserService::store($request->safe()->toArray());

        $user->sendWelcomeNotification(now()->addDay());

        return $request->wantsJson() ?
        response()->json([
            'message' => 'Usuario creado correctamente',
            'data' => $user,
        ]) :
        redirect()->back()->with('message', 'Usuario creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, User $admin)
    {
        $input = $request->safe()->toArray();
        UserService::update($admin, $input);

        return $request->wantsJson() ?
        response()->json([
            'message' => 'Usuario modificado correctamente',
            'data' => $admin,
        ]) :
        redirect()->back()->with('message', 'Usuario modificado correctamente');
    }
}
