<?php

namespace App\Http\Controllers;

use App\Enums\UserTypes;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Redirecciona a los usuarios a su módulo correspondiente.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(Request $request)
    {
        $redirect = auth()->user()->user_type == UserTypes::patient->value ? 'portal_patient.index' : 'portal_panel';

        return redirect()->route($redirect);
    }
}
