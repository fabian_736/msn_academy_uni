<?php

namespace App\Http\Controllers;

use App\Enums\UserTypes;
use App\Http\Requests\Patient\StoreRequest;
use App\Http\Requests\Patient\UpdateRequest;
use App\Models\Blog\Post;
use App\Models\DocumentType;
use App\Models\Patient;
use App\Services\Patient\PatientService;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::with('user')->get();
        $type_documents = DocumentType::all();

        return view('panel.user.client', compact(['type_documents', 'patients']));
    }

    public function profile_patient()
    {
        $patient = Patient::with('user')->get();

        return view('landing.patient.profile.index', compact('patient'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        /** @var Patient|null */
        $patient = null;

        DB::transaction(function () use ($request, &$patient) {
            $userInput = array_merge(
                $request->safe()->only(['name', 'surname', 'email']),
                ['user_type' => UserTypes::patient->value]
            );

            $patientInput = $request->safe()->only(['address', 'phone', 'document_type_id', 'document_number']);
            $user = UserService::store($userInput);
            $patient = $user->patient()->create($patientInput);

            $patient->load('user');
        }, 5);

        $patient->user->sendWelcomeNotification(now()->addDay());

        return $request->wantsJson() ?
            response()->json([
                'message' => 'Paciente creado correctamente',
                'data' => $patient,
            ]) :
            redirect()->back()->with('message', 'Paciente creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        $patient = Patient::with('user')->get();

        return $patient;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        // $patient = Patient::with('user')->get();
        $patient = Patient::with('user')->find(Auth::User()->id)->get();

        return $patient;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Patient $patient)
    {
        $request->safe()->merge([
            'user_type' => UserTypes::patient->value,
        ]);

        DB::transaction(function () use ($request, $patient) {
            $userInput = $request->safe()->only(['name', 'surname', 'user_type', 'email', 'is_active']);
            $patientInput = $request->safe()->only(['address', 'phone', 'document_type_id', 'document_number']);

            UserService::update($patient->user, $userInput);
            PatientService::update($patient, $patientInput);
        }, 5);

        return $request
            ->wantsJson() ?
            response()->json([
                'message' => 'Paciente modificado correctamente',
                'data' => $patient,
            ]) :
            redirect()->back()->with('message', 'Paciente modificado correctamente');
    }

    public function blog()
    {
        $posts = Post::with('author', 'comments.user')->orderBy('id', 'desc')->get();

        return view('landing.patient.blog.index', compact('posts'));
    }
}
