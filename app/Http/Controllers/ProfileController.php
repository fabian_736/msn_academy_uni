<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $users = User::with('patient')->get();

        return view('panel.perfil.index', compact('users'));
    }

    public function update(Request $request, $id)
    {
        $usuario = User::find(Auth::User()->id);
        $usuario->fill($request->all())->save();
        if ($request->hasFile('profile_image') && $request->file('profile_image')->isValid()) {
            $usuario->addMediaFromRequest('profile_image')->toMediaCollection('avatar');
        }

        return redirect()->back();
    }
}
