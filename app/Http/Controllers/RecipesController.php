<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecipeStoreRequest;
use App\Http\Requests\RecipeUpdateRequest;
use App\Models\Content\News;
use App\Models\Recipes;

class RecipesController extends Controller
{
    public function index()
    {
        $recipes = Recipes::with(['new', 'user'])->get();
        $news = News::orderBy('created_at', 'Desc')->get();

        return view('panel.contents.recipe.list_recipe', compact('recipes', 'news'));
    }

    public function store(RecipeStoreRequest $request)
    {
        $recipes = Recipes::create($request->all());
        $recipes->addMediaFromRequest('image')->toMediaCollection('banner');
        $recipes->addMediaFromRequest('file')->toMediaCollection('file');

        return redirect()->back();
    }

    public function edit($id)
    {
        $recipes = Recipes::FindOrFail($id);
    }

    public function update(RecipeUpdateRequest $request, $id)
    {
        $recipes = Recipes::FindOrFail($id);
        $recipes->fill($request->all())->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        Recipes::destroy($id);

        return redirect()->back();
    }
}
