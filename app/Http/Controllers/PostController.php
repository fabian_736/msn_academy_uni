<?php

namespace App\Http\Controllers;

use App\Http\Requests\Blog\Post\StoreRequest;
use App\Http\Requests\Blog\Post\UpdateRequest;
use App\Models\Blog\Comment;
use  App\Models\Blog\LikePost;
use App\Models\Blog\Post;
use App\Models\Content\News;
use App\Services\Blog\CommentService;
use App\Services\Blog\PostService;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    //

    public function list()
    {
        $posts = Post::orderBy('created_at', 'desc')->with('author')->get();

        return view('panel.blog.publications.publications', compact('posts'));
    }

    /**
     * @param  Request  $request
     * @param  Post  $post
     * @return [type]
     */
    public function patient_post_show(Request $request, Post $post)
    {
        $post->load(['comments.user', 'comments'=> function ($query) {
            return $query->where(function ($query) {
                return $query->where('user_id', Auth::user()->id)
                ->orWhere('active', true);
            });
        }]);
        $i_like_post = LikePost::where('post_id', $post->id)->where('user_id', Auth::user()->id)->first();

        return view('landing.patient.blog.post', compact('post', 'i_like_post'));
    }

    public function chat(Post $post)
    {
        $post = Post::orderBy('created_at', 'desc')->get();
        $post->load('comments.user');

        return view('landing.patient.blog.post_admin', compact('post'));
    }

    public function admin_post_store(Request $request)
    {
        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->active = 1;
        $comment->post_id = $request->post_id;
        $comment->user_id = Auth::user()->id;
        $comment->save();

        return redirect()->back();
    }

    /**
     * @param  Request  $request
     * @return [type]
     */
    public function patient_post_store(Request $request)
    {
        $comment = CommentService::store($request);

        return redirect()->route('blog_patient.show', $request->post_id)->with('message', 'comentario creado');
    }

    /**
     * @param  StoreRequest  $request
     * @return [type]
     */
    public function store(StoreRequest $request)
    {
        $input = $request->safe()->merge([
            'author_id' => Auth::user()->id,
        ]);
        $post = PostService::store($input->toArray());
        session()->flash('success', 'Publicación creada correctamente');

        return redirect()->route('blog.publications');
    }

    /**
     * @param  mixed  $id
     * @return [type]
     */
    public function edit($id)
    {
        $users = UserService::listAdmin();
        $new = News::findOrFail($id);

        return view('panel.contents.news_edit', compact('new', 'users'));
    }

    /**
     * @param  Request  $request
     * @param  mixed  $id
     * @return [type]
     */
    public function update(UpdateRequest $request, Post $post)
    {
        $input = $request->safe()->toArray();
        PostService::update($post, $input);

        return $request->wantsJson() ?
            response()->json([
                'message' => 'publicacion modificada correctamente',
                'data' => $post,
            ]) :
            redirect()->back()->with('message', 'publicacion modificada correctamente');
    }

    /**
     * @param  mixed  $id
     * @return [type]
     */
    public function destroy(Request $request, Post $post)
    {
        $post->delete();

        return $request->wantsJson() ?
            response()->json([
                'message' => 'publicacion eliminada correctamente',
                'data' => $post,
            ]) :
            redirect()->back()->with('message', 'publicacion eliminada correctamente');
    }

    public function patient_post_list()
    {
        $posts = Post::with('author', 'comments.user')->orderBy('id', 'desc')->paginate(5);

        return view('landing.patient.blog.index', compact('posts'));
    }

    /**
     * @param  Request  $request
     * @return [type]
     */
    public function patient_like(Request $request, Post $post)
    {
        if ($request->action == 'like') {
            $like_post = LikePost::where('user_id', Auth::user()->id)->where('post_id', $post->id)->first();
            if ($like_post) {
                $like_post->i_like_it = 1;
                $like_post->save();
            } else {
                $like_post = new LikePost();
                $like_post->post_id = $post->id;
                $like_post->user_id = Auth::user()->id;
                $like_post->i_like_it = 1;
                $like_post->save();
            }
        } else {
            $like_post = LikePost::where('user_id', Auth::user()->id)->where('post_id', $post->id)->first();
            if ($like_post) {
                $like_post->i_like_it = 0;
                $like_post->save();
            } else {
                $like_post = new LikePost();
                $like_post->post_id = $post->id;
                $like_post->user_id = Auth::user()->id;
                $like_post->i_like_it = 0;
                $like_post->save();
            }
        }

        return response()->json($like_post);
    }
}
