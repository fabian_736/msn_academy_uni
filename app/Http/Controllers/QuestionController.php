<?php

namespace App\Http\Controllers;

use App\Http\Requests\Question\StoreRequest;
use App\Http\Requests\Question\UpdateRequest;
use App\Models\Question;
use App\Models\Trainings\Course;
use App\Services\Training\QuestionService;
use App\Traits\JsonResponses;

class QuestionController extends Controller
{
    use JsonResponses;

    public function index(Course $course)
    {
        $course->load('questions.answer');

        return view('landing.patient.cuestionario.index', compact('course'));
    }

    /** @var QuestionService */
    private $service;

    public function store(StoreRequest $request)
    {
        $question = QuestionService::store($request->safe()->toArray());

        return $request->wantsJson() ?
            response()->json([
                'message' => 'Pregunta creada con exito',
                'data' => $question,
            ]) :
            redirect()->back()->with('status', 'Pregunta creada con exito');
    }

    public function update(UpdateRequest $request, $id)
    {
        $question = Question::FindOrFail($id);
        $question->fill($request->all())->save();

        return redirect()->back()->with('editada', 'Pregunta editada con exito');
    }

    public function destroy(Question $question)
    {
        QuestionService::destroy($question);

        return redirect()->back()->with('eliminada', 'Pregunta eliminada con exito');
    }
}
