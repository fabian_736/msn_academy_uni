<?php

namespace App\Http\Controllers;

use App\Http\Requests\Course\StoreRequest;
use App\Http\Requests\Course\UpdateRequest;
use App\Models\Trainings\Course;
use App\Models\Trainings\Training;
use App\Services\Training\CourseService;

class CourseController extends Controller
{
    /**
     * @return [type]
     */
    public function index()
    {
        $trainings = Training::orderBy('created_at', 'desc')->get();
        $courses = Course::with('media')->orderBy('status', 'desc')->get();

        return view('panel.trainings.list_courses', compact('courses', 'trainings'));
    }

    public function index_courses_patient()
    {
        $trainings = Training::orderBy('created_at', 'desc')->get();
        $courses = Course::with('media')->orderBy('created_at', 'desc')->get();

        return view('landing.patient.retos.trainings_details', compact('courses', 'trainings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $course = CourseService::store($request);
        $course->addMediaFromRequest('cur_banner')->toMediaCollection('banner');

        session()->flash('success', 'Curso creado correctamente');

        return redirect()->route('trainings.list_courses');
    }

    /**
     * Shows the form for edit the course.
     *
     * @param  Course  $course
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Course $course)
    {
        // $this->authorize('update', $course);

        $trainings = Training::orderBy('created_at', 'desc')->get();
        $course->load('questions.answers');

        return view('panel.trainings.courses_edit', compact('course', 'trainings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Course $course)
    {
        // $this->authorize('update', $course);
        $course = CourseService::update($request, $course);
        session()->flash('success', 'Curso actualizado correctamente');

        return redirect()->route('trainings.list_courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $this->authorize('delete', $course);

        foreach ($course->media as $media) {
            $media->delete();
        }
        $course->delete();
        session()->flash('success', 'Curso eliminado correctamente');

        return redirect()->route('trainings.list_courses');
    }
}
