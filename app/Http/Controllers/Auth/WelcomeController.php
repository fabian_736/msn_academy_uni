<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Fortify\PasswordValidationRules;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\WelcomeNotification\WelcomeController as BaseWelcomeController;

class WelcomeController extends BaseWelcomeController
{
    use PasswordValidationRules;

    /**
     * Almacena la contraseña del usuario.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function savePassword(Request $request, User $user)
    {
        $request->validate($this->rules());

        $user->password = Hash::make($request->password);
        $user->welcome_valid_until = null;
        $user->email_verified_at = now();
        $user->save();

        return $this->sendPasswordSavedResponse();
    }

    /**
     * Reglas de validación de la contraseña.
     *
     * @return aray
     */
    protected function rules(): array
    {
        return [
            'password' => $this->passwordRules(),
        ];
    }

    /**
     * Retorna la url a la que debe ser redireccionado el usuario después de establecer su contraseña.
     *
     * @return string
     */
    public function redirectTo(): string
    {
        return route('index');
    }
}
