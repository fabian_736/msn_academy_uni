<?php

namespace App\Http\Responses;

use App\Enums\UserTypes;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        $redirect = $request->user()->user_type == UserTypes::patient->value ? 'portal_patient.index' : 'portal_panel';

        return $request->wantsJson()
            ? response()->json(['two_factor' => false])
            : redirect()->intended(route($redirect));
    }
}
