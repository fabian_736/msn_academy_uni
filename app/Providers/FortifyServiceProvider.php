<?php

namespace App\Providers;

use App\Actions\Fortify\AuthenticateLoginAttempt;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Login
        Fortify::loginView(function () {
            return view('auth.login');
        });

        Fortify::authenticateUsing([new AuthenticateLoginAttempt, '__invoke']);

        $this->app->singleton(
            \Laravel\Fortify\Contracts\LoginResponse::class,
            \App\Http\Responses\LoginResponse::class
        );

        RateLimiter::for('login', function (Request $request) {
            $email = (string) $request->email;

            return Limit::perMinute(5)->by($email.$request->ip());
        });

        // Reset password
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);
        Fortify::requestPasswordResetLinkView(function () {
            return view('auth.forgot-password');
        });

        Fortify::resetPasswordView(function ($request) {
            return view('auth.reset-password', ['request' => $request]);
        });

        // Verify email
        Fortify::verifyEmailView(function () {
            return view('auth.verify-email');
        });

        // Update password
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);

        // Update profile
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
    }
}
