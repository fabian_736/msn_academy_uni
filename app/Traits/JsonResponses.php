<?php

namespace App\Traits;

use Illuminate\Http\Resources\Json\JsonResource;

trait JsonResponses
{
    /**
     * Retorna un JsonResource estandarizado.
     *
     * @param  string  $message
     * @param  JsonResource  $data
     * @param  int  $code
     * @param  array  $metadata
     * @return \Illuminate\Http\JsonResponse
     */
    public function resourceResponse(string $message, JsonResource $data, int $code = 200, $metadata = [])
    {
        $metadata = array_merge([
            'message' => $message,
            'status' => $code,
        ], $metadata);

        return $data->additional($metadata)
            ->response()
            ->setStatusCode($code);
    }

    /**
     * Retorna una respuesta json estandarizada.
     *
     * @param  string  $message
     * @param  array  $data
     * @param  int  $code
     * @param  array  $metadata
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonResponse(string $message, array $data = [], int $code = 200, $metadata = [])
    {
        $defaultResponse = [
            'message' => $message,
            'status' => $code,
            'data' => $data,
        ];

        return response()->json(array_merge($defaultResponse, $metadata), $code);
    }
}
